# README #

This is the code base for a web application that manages case-report forms for the ECPBOS Clinical Trial.

A partial list of supported features:
1. Multiple sites
2. Access control based on users and roles
3. Managed form state: submitted, questioned, verified
4. Uploading of associated supporting documents.

While this code base has the potential to provide full support for general clinical trials, it is currently coupled closely to this particular trial.

This repo begins with the migration of version 5.0.4 at https://code.imphub.org/projects/ECPR/repos/repo


### How do I get set up? ###

Java 1.7
JSF 2.2 running in Tomcat7
JDBC connection to RDMS: We are using Postgresql 9
LDAP Directory for managing users and roles: We are using OpenDJ

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions