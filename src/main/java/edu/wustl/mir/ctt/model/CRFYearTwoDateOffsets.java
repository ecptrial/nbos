/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.wustl.mir.ctt.model;

/**
 *
 * @author Lauren Wallace
 */
public class CRFYearTwoDateOffsets {
    private final int[] currentTherapyDateOffsets = {0, 820, 910, 1000, 1095};
    private final String[] currentTherapyNames = {"", "Year Three Day 90", "Year Three Day 180", "Year Three Day 270", "Year Three Day 365"};

    private final int[] pulmonaryEvalDateOffsets = {0, 790, 850, 910, 970, 1030, 1095};
    private final int[] pulmonaryEvalLastDateOffsets = {0, 15, 15, 15, 15, 15, 15};
    private final String[] pulmonaryEvalNames = {"", "Year Three Day 60", "Year Three Day 120", "Year Three Day 180", "Year Three Day 240", "Year Three Day 300", "Year Three Day 365"};
    
    public int[] getCurrentTherapyDateOffsets() {
        return currentTherapyDateOffsets;
    }
    
    public int getCurrentTherapyDateOffset(int i) {
        return currentTherapyDateOffsets[i];
    }
    
    public String getCurrentTherapyName(int i) {
        return currentTherapyNames[i];
    }

    public int[] getPulmonaryEvalDateOffsets() {
        return pulmonaryEvalDateOffsets;
    }
    
    public int[] getPulmonaryEvalLastDateOffsets() {
        return pulmonaryEvalLastDateOffsets;
    }
    
    public int getPulmonaryEvalDateOffset(int i) {
        return pulmonaryEvalDateOffsets[i];
    }
    
    public int getPulmonaryEvalLastDateOffset(int i) {
        return pulmonaryEvalLastDateOffsets[i];
    }
    
    public String getPulmonaryEvalName(int i) {
        return pulmonaryEvalNames[i];
    }
}
