/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.wustl.mir.ctt.model;

/**
 *
 * @author Lauren Wallace
 */
public class CRFYearOneDateOffsets {
    private final int[] currentTherapyDateOffsets = {0, 455, 545, 635, 730};
    private final String[] currentTherapyNames = {"", "Year Two Day 90", "Year Two Day 180", "Year Two Day 270", "Year Two Day 365"};

    private final int[] pulmonaryEvalDateOffsets = {0, 425, 485, 545, 605, 665, 730};
    private final int[] pulmonaryEvalLastDateOffsets = {0, 15, 15, 15, 15, 15, 15};
    private final String[] pulmonaryEvalNames = {"", "Year Two Day 60", "Year Two Day 120", "Year Two Day 180", "Year Two Day 240", "Year Two Day 300", "Year Two Day 365"};
    
    public int[] getCurrentTherapyDateOffsets() {
        return currentTherapyDateOffsets;
    }
    
    public int getCurrentTherapyDateOffset(int i) {
        return currentTherapyDateOffsets[i];
    }
    
    public String getCurrentTherapyName(int i) {
        return currentTherapyNames[i];
    }

    public int[] getPulmonaryEvalDateOffsets() {
        return pulmonaryEvalDateOffsets;
    }
    
    public int[] getPulmonaryEvalLastDateOffsets() {
        return pulmonaryEvalLastDateOffsets;
    }
    
    public int getPulmonaryEvalDateOffset(int i) {
        return pulmonaryEvalDateOffsets[i];
    }
    
    public int getPulmonaryEvalLastDateOffset(int i) {
        return pulmonaryEvalLastDateOffsets[i];
    }
    
    public String getPulmonaryEvalName(int i) {
        return pulmonaryEvalNames[i];
    }
}
