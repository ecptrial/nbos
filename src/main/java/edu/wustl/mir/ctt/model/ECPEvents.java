package edu.wustl.mir.ctt.model;

import edu.wustl.mir.ctt.form.AdverseEventWorksheetSAEForm;
import edu.wustl.mir.ctt.form.AnnualFollowUpForm;
import edu.wustl.mir.ctt.form.BaselineTherapyForm;
import edu.wustl.mir.ctt.form.BasicForm;
import edu.wustl.mir.ctt.form.ChangeTherapyForm;
import edu.wustl.mir.ctt.form.CrossoverEligibilityWorkSheet;
import edu.wustl.mir.ctt.form.CrossoverSafetyCheckForm;
import edu.wustl.mir.ctt.form.CurrentTherapyForm;
import edu.wustl.mir.ctt.form.DemoMedHistForm;
import edu.wustl.mir.ctt.form.ECPTreatmentForm;
import edu.wustl.mir.ctt.form.EligibilityForm;
import edu.wustl.mir.ctt.form.EndOfStudyForm;
import edu.wustl.mir.ctt.form.HospitalizationForm;
import edu.wustl.mir.ctt.form.QualityOfLifeForm;
import edu.wustl.mir.ctt.form.ObservePulmEvalLogForm;
import edu.wustl.mir.ctt.form.PulmEvalForm;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.PersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author drm
 */
public class ECPEvents {
    
    public static Event getInstance( ECPEventTypes type) {
        Event e = new Event(null);
        e.setName(type.getName());
        e.setType(type);
        e.setStatus(EventStatus.NEW);
        e.setExpected(false);
        e.setActualDate( new Date());
        return e;
    }
    
    /**
     * Determine the label for a new instance of the specified event type.
     * 
     * This label will likely depend on the events that already exist for
     * the specified participant.
     * 
     * Only SAEs have non-null labels.
     * 
     * @param type
     * @param p
     * @return
     * @throws PersistenceException 
     */
    public static String getLabel( ECPEventTypes type, Participant p) throws PersistenceException {
        String label = null;

        switch (type) {
            case SERIOUS_ADVERSE_EVENT:
                PersistenceManager pm = ServiceRegistry.getPersistenceManager();
                List<Event> events = pm.getEvents(p);
                int count = 0;
                for( Event e: events) {
                    if( ECPEventTypes.SERIOUS_ADVERSE_EVENT.equals( e.getType())) {
                        count++;
                    }
                }
                label = "SAE - " + (count+1);
                break;
            default:
                break;
        }

        return label;
    }
    
    public static List<BasicForm> getForms( ECPEventTypes type) {
        List<BasicForm> forms = new ArrayList<BasicForm>();
        BasicForm f;
        
        switch( type) {
            case ELIGIBILITY:
//                SimpleEligibilityForm sf = new SimpleEligibilityForm();
                EligibilityForm sf = new EligibilityForm();
                forms.add(sf);
                break;
            case DEMOGRAPHICS:
                f = new DemoMedHistForm();
                forms.add(f);
                break;
            case ECP_TREATMENT:
                f = new ECPTreatmentForm();
                forms.add(f);
                break;
            case PULMONARY_EVAL:
                f = new PulmEvalForm();
//                System.out.println("The PULMONARY_EVAL form title is: " + f.getTitle());
                forms.add(f);
                break;
            case BASELINE_THERAPY:
                f = new BaselineTherapyForm();
//                System.out.println("The CHANGE_THERAPY form title is: " + f.getTitle());
                forms.add(f);
                break;
            case CHANGE_THERAPY:
                f = new ChangeTherapyForm();
//                System.out.println("The CHANGE_THERAPY form title is: " + f.getTitle());
                forms.add(f);
                break;
            case ADVERSE_EVENT_WORKSHEET_SAE:
                f = new AdverseEventWorksheetSAEForm();
                forms.add(f);
                break;
            case SERIOUS_ADVERSE_EVENT:
                f = new AdverseEventWorksheetSAEForm();
                forms.add(f);
                break;
            case END_OF_STUDY:
                f = new EndOfStudyForm();
                forms.add(f);
                break;
            case QUALITY_OF_LIFE:
                f = new QualityOfLifeForm();
                forms.add(f);
                break;
            case ANNUAL_FOLLOW_UP:
                f = new AnnualFollowUpForm();
                forms.add(f);
                break;
            case CROSSOVER_ELIGIBILITY_WORKSHEET:
                f = new CrossoverEligibilityWorkSheet();
                forms.add(f);
                break;
            case CROSSOVER_SAFETY_CHECK:
                f = new CrossoverSafetyCheckForm();
                forms.add(f);
                break;
           // case HOSPITALIZATION:
           //     f = new HospitalizationForm();
           //     forms.add(f);
           //     break;
            case CURRENT_THERAPY:
                f = new CurrentTherapyForm();
                forms.add(f);
                break; 
            default:
                BasicForm bf = new BasicForm();
//                System.out.println("The default BASIC Form title is: " + bf.getTitle());
                forms.add(bf);
        }
        
        return forms;
    }
    
    // This method uses the formTitle because for some CRFs there are multiple CRFs with different titles.
    // The different title needs to be set in the new CRF that is created.
    public static List<BasicForm> getForms( ECPEventTypes type, String formTitle) {
        List<BasicForm> forms = new ArrayList<BasicForm>();
        BasicForm f;
        
        switch( type) {
            case ELIGIBILITY:
//                SimpleEligibilityForm sf = new SimpleEligibilityForm();
                EligibilityForm sf = new EligibilityForm();
                forms.add(sf);
                break;
            case DEMOGRAPHICS:
                f = new DemoMedHistForm();
                forms.add(f);
                break;
            case ECP_TREATMENT:
                f = new ECPTreatmentForm();
                f.setTitle(formTitle);
                forms.add(f);
                break;
            case PULMONARY_EVAL:
                f = new PulmEvalForm();
                f.setTitle(formTitle);
                forms.add(f);
                break;
            case BASELINE_THERAPY:
                f = new BaselineTherapyForm();
                f.setTitle(formTitle);
                forms.add(f);
                break;
            case CHANGE_THERAPY:
                f = new ChangeTherapyForm();
                f.setTitle(formTitle);
                forms.add(f);
                break;
            case ADVERSE_EVENT_WORKSHEET_SAE:
                f = new AdverseEventWorksheetSAEForm();
                f.setTitle(formTitle);
                forms.add(f);
                break;
            case END_OF_STUDY:
                f = new EndOfStudyForm();
                forms.add(f);
                break;
            case QUALITY_OF_LIFE:
                f = new QualityOfLifeForm();
                f.setTitle(formTitle);
                forms.add(f);
                break;
            case ANNUAL_FOLLOW_UP:
                f = new AnnualFollowUpForm();
                forms.add(f);
                break;
            case OBSERVATION_PULMONARY_EVAL_LOG:
                f = new ObservePulmEvalLogForm();
                forms.add(f);
                break;
            case CURRENT_THERAPY:
                f = new CurrentTherapyForm();
                forms.add(f);
                break; 
            default:
                BasicForm bf = new BasicForm();
                forms.add(bf);
        }
        
        return forms;
    }
    
    public static List<Event> getEPIScheduledEvents( Participant participant) {
        List<Event> newEvents = new ArrayList<Event>();
        List<BasicForm> forms = new ArrayList<BasicForm>();
        ECPVisitDateOffsets ecpVisitDateOffsets = new ECPVisitDateOffsets();
//        ECPVisitLastDateOffsets ecpVisitLastDateOffsets = new ECPVisitLastDateOffsets();
        PulmEvalVisitDateOffsets pulmEvalVisitDateOffsets = new PulmEvalVisitDateOffsets();
        PulmEvalVisitLastDateOffsets pulmEvalVisitLastDateOffsets = new PulmEvalVisitLastDateOffsets();
        ChangeTherapyDateOffsets changeTherapyDateOffsets = new ChangeTherapyDateOffsets();
        QualityOfLifeDateOffsets qualityOfLifeDateOffsets = new QualityOfLifeDateOffsets();
        
        Event e = new Event(null);
        e.setType(ECPEventTypes.DEMOGRAPHICS);
        e.setName("Demographics/Medical History");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        // The following statment returns just a NEW Demographics Form.
        forms = ECPEvents.getForms(ECPEventTypes.DEMOGRAPHICS);
        // Get the Demographics form (get(0)) and set the date to be the enrollment date.
        forms.get(0).setDate(participant.getEnrolledDate());
        e.setForms(forms);
        e.setExpected(false);
//        e.setBaseDate(new Date());  // Since the Confirmation of Eligibility form cannot be completed excepted when all of the eligibility criteria are accepted
                                    // so the date of when the participant was determined to be eligible will always be the date when the confirmation occurred.
                                    // Therefore, today's date makes sense for setting the Actual Date Confirmation of Eligibility form and for settig the basedate
                                    // for the Demographics form which can not be completed because the participant is enrolled in the study now.
        e.setOffsetFromBaseDateInDays(0);
        e.setActualDate(participant.getEnrolledDate()); // The demographics actual date will be identical to the Confirmation of Eligibility actual date because the 
                                    // demographics form uses the participant's enrollment date as the date.
        newEvents.add(e);

        forms = new ArrayList<BasicForm>();
        e = new Event(null);
        e.setType(ECPEventTypes.BASELINE_THERAPY);
        e.setName("Baseline Therapy");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        // The following statement returns just a NEW Baseline Therapy form.
        forms = ECPEvents.getForms(ECPEventTypes.BASELINE_THERAPY);
        // Get the Baseline Therapy form (get(0)) and set the date to be the Baseline Therapy date.
        forms.get(0).setDate(participant.getEnrolledDate());
        e.setForms(forms);
        e.setExpected(false);
        e.setOffsetFromBaseDateInDays(0);
        e.setActualDate(participant.getEnrolledDate()); // The Baseline Therapy actual date will be identical to the Confirmation of Eligibility actual date because the 
                                    // Baseline Therapy form uses the participant's enrollment date as the date.
        newEvents.add(e);
//        
//        e = new Event(null);
//        e.setType(ECPEventTypes.ECP_TREATMENT.getName());
//        e.setName("ECP Treatment 1");
//        e.setParticipantId(participantID);
//        e.setStatus(EventStatus.PENDING);
//        e.setForms(ECPEvents.getForms(ECPEventTypes.ECP_TREATMENT));
//        e.setExpected(true);
//        newEvents.add(e);
//        
//        e = new Event(null);
//        e.setType(ECPEventTypes.ECP_TREATMENT.getName());
//        e.setName("ECP Treatment 2");
//        e.setParticipantId(participantID);
//        e.setStatus(EventStatus.PENDING);
//        e.setForms(ECPEvents.getForms(ECPEventTypes.ECP_TREATMENT));
//        e.setExpected(true);
//        e.setOffsetFromBaseDateInDays(3);
//        newEvents.add(e);
//        
//        e = new Event(null);
//        e.setType(ECPEventTypes.ECP_TREATMENT.getName());
//        e.setName("ECP Treatment 3");
//        e.setParticipantId(participantID);
//        e.setStatus(EventStatus.PENDING);
//        e.setForms(ECPEvents.getForms(ECPEventTypes.ECP_TREATMENT));
//        e.setExpected(true);
//        e.setOffsetFromBaseDateInDays(6);
//        newEvents.add(e);

        for(int i = 1; i < ecpVisitDateOffsets.getDateOffsets().length; i++){
            String formTitle = "ECP Treatment Visit " + i + " Form";
            e = new Event(null);
            e.setType(ECPEventTypes.ECP_TREATMENT);
            e.setName("ECP Treatment " + i);
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.ECP_TREATMENT, formTitle));
            e.setExpected(true);
            e.setOffsetFromBaseDateInDays(ecpVisitDateOffsets.getDateOffset(i));
            e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
//            System.out.println("The showLastAvailableDate value is: " + e.getShowLastAvailableDate());
//            e.setOffsetToLastAvailableDayInDays(ecpVisitLastDateOffsets.getLastDateOffset(i));
            newEvents.add(e);
        }
        
//        e = new Event(null);
//        e.setType(ECPEventTypes.PULMONARY_EVAL.getName());
//        e.setName("Pulmonary Eval: 30 day assessment");
//        e.setParticipantId(participantID);
//        e.setStatus(EventStatus.PENDING);
//        e.setForms(ECPEvents.getForms(ECPEventTypes.PULMONARY_EVAL));
//        e.setExpected(true);
//        e.setOffsetFromBaseDateInDays(30);
//        newEvents.add(e);
        
        for(int i = 1; i < pulmEvalVisitDateOffsets.getDateOffsets().length; i++){
            String formTitle = "Pulmonary Evaluation: " + pulmEvalVisitDateOffsets.getDateOffset(i) + " Day Assessment Form";
            e = new Event(null);
            e.setType(ECPEventTypes.PULMONARY_EVAL);
            e.setName("Pulmonary Evaluation: " + pulmEvalVisitDateOffsets.getDateOffset(i) + " day assessment");
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.PULMONARY_EVAL, formTitle));
            e.setExpected(true);
            e.setOffsetFromBaseDateInDays(pulmEvalVisitDateOffsets.getDateOffset(i));
            e.setOffsetToLastAvailableDayInDays(pulmEvalVisitLastDateOffsets.getLastDateOffset(i));
            newEvents.add(e);
        }
        
        for(int i = 1; i < changeTherapyDateOffsets.getDateOffsets().length; i++){
            String formTitle = "Change in Therapy Day " + changeTherapyDateOffsets.getDateOffset(i) + " Form";
            e = new Event(null);
            e.setType(ECPEventTypes.CHANGE_THERAPY);
            e.setName("Change in Therapy Day " + changeTherapyDateOffsets.getDateOffset(i));
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.CHANGE_THERAPY, formTitle));
            e.setExpected(true);
            e.setOffsetFromBaseDateInDays(changeTherapyDateOffsets.getDateOffset(i));
            e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
            newEvents.add(e);
        }
        
        for(int i = 1; i < qualityOfLifeDateOffsets.getDateOffsets().length; i++){
            int dateOffset = qualityOfLifeDateOffsets.getDateOffset(i);
            String formTitle;
            
            e = new Event(null);
            e.setType(ECPEventTypes.QUALITY_OF_LIFE);
            
            if (dateOffset == 0) {
                formTitle = "Quality of Life Baseline Form";
                e.setName("Quality of Life Baseline");
            } else {
                formTitle = "Quality of Life Day " + qualityOfLifeDateOffsets.getDateOffset(i) + " Form";
                e.setName("Quality of Life Day " + qualityOfLifeDateOffsets.getDateOffset(i));
            }
            
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.QUALITY_OF_LIFE, formTitle));
            e.setExpected(true);
            e.setOffsetFromBaseDateInDays(qualityOfLifeDateOffsets.getDateOffset(i));
            e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
            newEvents.add(e);
        }
        
        e = new Event(null);
        e.setType(ECPEventTypes.END_OF_STUDY);
        e.setName("End Of Study");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.END_OF_STUDY));
        e.setExpected(true);
//        e.setOffsetFromBaseDateInDays(393);
//        e.setOffsetToLastAvailableDayInDays(15);
        newEvents.add(e);
        
//        e = new Event(null);
//        e.setType(ECPEventTypes.ANNUAL_FOLLOW_UP);
//        e.setName("Annual Follow-Up");
//        e.setParticipantId(participant.getId());
//        e.setStatus(EventStatus.NEW);
//        e.setForms(ECPEvents.getForms(ECPEventTypes.ANNUAL_FOLLOW_UP));
//        e.setExpected(true);
////        e.setOffsetFromBaseDateInDays(393);
////        e.setOffsetToLastAvailableDayInDays(15);
//        newEvents.add(e);

        String formTitle = "Annual Follow Up Year Two Log";
        e = new Event(null);
        e.setType(ECPEventTypes.ANNUAL_FOLLOW_UP);
        e.setName("Annual Follow Up Year Two Log");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.ANNUAL_FOLLOW_UP, formTitle));
        e.setExpected(true);
        e.setBaseDate(participant.getEnrolledDate());

        //set actual date
        Date enrollmentDate = participant.getStudyArmEnrollDate();    
        Calendar c = Calendar.getInstance();
        c.setTime(enrollmentDate);
        c.add(Calendar.DATE, 545);
        Date startDate = c.getTime();
        e.setActualDate(startDate);

        e.setOffsetFromBaseDateInDays(366);
        e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
        newEvents.add(e);

        return newEvents;
    }

    public static List<Event> getControlScheduledEvents( Participant participant) {
        List<Event> newEvents = new ArrayList<Event>();
        List<BasicForm> forms = new ArrayList<BasicForm>();
//        ECPVisitLastDateOffsets ecpVisitLastDateOffsets = new ECPVisitLastDateOffsets();
        PulmEvalVisitDateOffsets pulmEvalVisitDateOffsets = new PulmEvalVisitDateOffsets();
        PulmEvalVisitLastDateOffsets pulmEvalVisitLastDateOffsets = new PulmEvalVisitLastDateOffsets();
        ChangeTherapyDateOffsets changeTherapyDateOffsets = new ChangeTherapyDateOffsets();
        QualityOfLifeDateOffsets qualityOfLifeDateOffsets = new QualityOfLifeDateOffsets();
        
        Event e = new Event(null);
        e.setType(ECPEventTypes.DEMOGRAPHICS);
        e.setName("Demographics/Medical History");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        // The following statment returns just a NEW Demographics Form.
        forms = ECPEvents.getForms(ECPEventTypes.DEMOGRAPHICS);
        // Get the Demographics form (get(0)) and set the date to be the enrollment date.
        forms.get(0).setDate(participant.getEnrolledDate());
        e.setForms(forms);
        e.setExpected(false);
//        e.setBaseDate(new Date());  // Since the Confirmation of Eligibility form cannot be completed excepted when all of the eligibility criteria are accepted
                                    // so the date of when the participant was determined to be eligible will always be the date when the confirmation occurred.
                                    // Therefore, today's date makes sense for setting the Actual Date Confirmation of Eligibility form and for settig the basedate
                                    // for the Demographics form which can not be completed because the participant is enrolled in the study now.
        e.setOffsetFromBaseDateInDays(0);
        e.setActualDate(participant.getEnrolledDate()); // The demographics actual date will be identical to the Confirmation of Eligibility actual date because the 
                                    // demographics form uses the participant's enrollment date as the date.
        newEvents.add(e);

        forms = new ArrayList<BasicForm>();
        e = new Event(null);
        e.setType(ECPEventTypes.BASELINE_THERAPY);
        e.setName("Baseline Therapy");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        // The following statement returns just a NEW Baseline Therapy form.
        forms = ECPEvents.getForms(ECPEventTypes.BASELINE_THERAPY);
        // Get the Baseline Therapy form (get(0)) and set the date to be the Baseline Therapy date.
        forms.get(0).setDate(participant.getEnrolledDate());
        e.setForms(forms);
        e.setExpected(false);
        e.setOffsetFromBaseDateInDays(0);
        e.setActualDate(participant.getEnrolledDate()); // The Baseline Therapy actual date will be identical to the Confirmation of Eligibility actual date because the 
                                    // Baseline Therapy form uses the participant's enrollment date as the date.
        newEvents.add(e);
        
        for(int i = 1; i < pulmEvalVisitDateOffsets.getDateOffsets().length; i++){
            String formTitle = "Pulmonary Evaluation: " + pulmEvalVisitDateOffsets.getDateOffset(i) + " Day Assessment Form";
            e = new Event(null);
            e.setType(ECPEventTypes.PULMONARY_EVAL);
            e.setName("Pulmonary Evaluation: " + pulmEvalVisitDateOffsets.getDateOffset(i) + " day assessment");
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.PULMONARY_EVAL, formTitle));
            e.setExpected(true);
            e.setOffsetFromBaseDateInDays(pulmEvalVisitDateOffsets.getDateOffset(i));
            e.setOffsetToLastAvailableDayInDays(pulmEvalVisitLastDateOffsets.getLastDateOffset(i));
            newEvents.add(e);
        }
        
        for(int i = 1; i < changeTherapyDateOffsets.getDateOffsets().length; i++){
            String formTitle = "Change in Therapy Day " + changeTherapyDateOffsets.getDateOffset(i) + " Form";
            e = new Event(null);
            e.setType(ECPEventTypes.CHANGE_THERAPY);
            e.setName("Change in Therapy Day " + changeTherapyDateOffsets.getDateOffset(i));
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.CHANGE_THERAPY, formTitle));
            e.setExpected(true);
            e.setOffsetFromBaseDateInDays(changeTherapyDateOffsets.getDateOffset(i));
            e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
            newEvents.add(e);
        }
        
        for(int i = 1; i < qualityOfLifeDateOffsets.getDateOffsets().length; i++){
            int dateOffset = qualityOfLifeDateOffsets.getDateOffset(i);
            String formTitle;
            
            e = new Event(null);
            e.setType(ECPEventTypes.QUALITY_OF_LIFE);
            
            if (dateOffset == 0) {
                formTitle = "Quality of Life Baseline Form";
                e.setName("Quality of Life Baseline");
            } else {
                formTitle = "Quality of Life Day " + qualityOfLifeDateOffsets.getDateOffset(i) + " Form";
                e.setName("Quality of Life Day " + qualityOfLifeDateOffsets.getDateOffset(i));
            }
            
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.QUALITY_OF_LIFE, formTitle));
            e.setExpected(true);
            e.setOffsetFromBaseDateInDays(qualityOfLifeDateOffsets.getDateOffset(i));
            e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
            newEvents.add(e);
        }
        
        e = new Event(null);
        e.setType(ECPEventTypes.END_OF_STUDY);
        e.setName("End Of Study");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.END_OF_STUDY));
        e.setExpected(true);
        //e.setOffsetFromBaseDateInDays(393);
        //e.setOffsetToLastAvailableDayInDays(15);
        newEvents.add(e);
        
        String formTitle = "Annual Follow Up Year Two Log";
        e = new Event(null);
        e.setType(ECPEventTypes.ANNUAL_FOLLOW_UP);
        e.setName("Annual Follow Up Year Two Log");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.ANNUAL_FOLLOW_UP, formTitle));
        e.setExpected(true);
        e.setBaseDate(participant.getEnrolledDate());

        //set actual date
        Date enrollmentDate = participant.getStudyArmEnrollDate();    
        Calendar c = Calendar.getInstance();
        c.setTime(enrollmentDate);
        c.add(Calendar.DATE, 545);
        Date startDate = c.getTime();
        e.setActualDate(startDate);

        e.setOffsetFromBaseDateInDays(366);
        e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
        newEvents.add(e);
        
        return newEvents;
    }
    
    public static List<Event> getCrossoverScheduledTreatmentEvents( int participantID) {
        List<Event> newEvents = new ArrayList<Event>();
        ECPVisitDateOffsets ecpVisitDateOffsets = new ECPVisitDateOffsets();
        
        Event e;

        for(int i = 1; i < ecpVisitDateOffsets.getDateOffsets().length; i++){
            String formTitle = "ECP Treatment Visit " + i + " Form";
            e = new Event(null);
            e.setType(ECPEventTypes.ECP_TREATMENT);
            e.setName("ECP Treatment " + i);
            e.setParticipantId(participantID);
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.ECP_TREATMENT, formTitle));
            e.setExpected(true);
            e.setOffsetFromBaseDateInDays(ecpVisitDateOffsets.getDateOffset(i));
            e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
//            System.out.println("The showLastAvailableDate value is: " + e.getShowLastAvailableDate());
//            e.setOffsetToLastAvailableDayInDays(ecpVisitLastDateOffsets.getLastDateOffset(i));
            newEvents.add(e);
        }

        return newEvents;
    }

        public static List<Event> getSixMonthEvents(Participant participant) {
        List<Event> newEvents = new ArrayList<>();
        CRFSixMonthDateOffsets crfSixMonthDateOffsets = new CRFSixMonthDateOffsets();
        Event e;
        
        for(int i = 1; i < crfSixMonthDateOffsets.getPulmEvalDateOffsets().length; i++){
            String formTitle = "Pulmonary Evaluation: " + crfSixMonthDateOffsets.getPulmEvalDateOffset(i) + " Day Assessment Form";
            e = new Event(null);
            e.setType(ECPEventTypes.PULMONARY_EVAL);
            e.setName("Pulmonary Evaluation: " + crfSixMonthDateOffsets.getPulmEvalDateOffset(i) + " day assessment");
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.PULMONARY_EVAL, formTitle));
            e.setExpected(true);
            e.setBaseDate(participant.getEnrolledDate());

            e.setOffsetFromBaseDateInDays(crfSixMonthDateOffsets.getPulmEvalDateOffset(i));
            e.setOffsetToLastAvailableDayInDays(crfSixMonthDateOffsets.getPulmEvalLastDateOffset(i));
            newEvents.add(e);
        }
        
        for(int i = 1; i < crfSixMonthDateOffsets.getChangeTherapyDateOffsets().length; i++){
            String formTitle = "Change in Therapy Day " + crfSixMonthDateOffsets.getChangeTherapyDateOffset(i) + " Form";
            e = new Event(null);
            e.setType(ECPEventTypes.CHANGE_THERAPY);
            e.setName("Change in Therapy Day " + crfSixMonthDateOffsets.getChangeTherapyDateOffset(i));
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.CHANGE_THERAPY, formTitle));
            e.setExpected(true);
            e.setBaseDate(participant.getEnrolledDate());
            e.setOffsetFromBaseDateInDays(crfSixMonthDateOffsets.getChangeTherapyDateOffset(i));
            e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
            newEvents.add(e);
        }
        
        for(int i = 1; i < crfSixMonthDateOffsets.getQualityOfLifeDateOffsets().length; i++){
            String formTitle = "Quality of Life Day " + crfSixMonthDateOffsets.getQualityOfLifeDateOffset(i) + " Form";
            e = new Event(null);
            e.setType(ECPEventTypes.QUALITY_OF_LIFE);
            e.setName("Quality of Life Day " + crfSixMonthDateOffsets.getQualityOfLifeDateOffset(i));
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.QUALITY_OF_LIFE, formTitle));
            e.setExpected(true);
            e.setBaseDate(participant.getEnrolledDate());
            e.setOffsetFromBaseDateInDays(crfSixMonthDateOffsets.getQualityOfLifeDateOffset(i));
            e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
            newEvents.add(e);
        }
        
        return newEvents;
    }
    
    public static List<Event> getYearOneEvents(Participant participant) {
        List<Event> newEvents = new ArrayList<>();
        CRFYearOneDateOffsets crfYearOneDateOffsets = new CRFYearOneDateOffsets();
        Event e = new Event(null);
        String formTitle;
        
        formTitle = "Annual Follow Up Year Two Log";
        e = new Event(null);
        e.setType(ECPEventTypes.ANNUAL_FOLLOW_UP);
        e.setName("Annual Follow Up Year Two Log");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.ANNUAL_FOLLOW_UP, formTitle));
        e.setExpected(true);
        e.setBaseDate(participant.getEnrolledDate());
        
        //set actual date
        Date enrollmentDate = participant.getStudyArmEnrollDate();    
        Calendar c = Calendar.getInstance();
        c.setTime(enrollmentDate);
        c.add(Calendar.DATE, 545);
        Date startDate = c.getTime();
        e.setActualDate(startDate);
        
        e.setOffsetFromBaseDateInDays(366);
        e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
        newEvents.add(e);
        
        for(int i = 1; i < crfYearOneDateOffsets.getCurrentTherapyDateOffsets().length; i++){
            formTitle = "Current Therapy " + crfYearOneDateOffsets.getCurrentTherapyDateOffset(i) + " Form";
            e = new Event(null);
            e.setType(ECPEventTypes.CURRENT_THERAPY);
            e.setName("Current Therapy " + crfYearOneDateOffsets.getCurrentTherapyName(i));
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.CURRENT_THERAPY, formTitle));
            e.setExpected(true);
            e.setBaseDate(participant.getEnrolledDate());
            e.setOffsetFromBaseDateInDays(crfYearOneDateOffsets.getCurrentTherapyDateOffset(i));
            e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
            newEvents.add(e);
        }
        
        for(int i = 1; i < crfYearOneDateOffsets.getPulmonaryEvalDateOffsets().length; i++){
            formTitle = "Pulmonary Evaluation: " + crfYearOneDateOffsets.getPulmonaryEvalName(i) + " Form";
            e = new Event(null);
            e.setType(ECPEventTypes.PULMONARY_EVAL);
            e.setName("Pulmonary Evaluation: " + crfYearOneDateOffsets.getPulmonaryEvalName(i));
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.PULMONARY_EVAL, formTitle));
            e.setExpected(true);
            e.setBaseDate(participant.getEnrolledDate());
            e.setOffsetFromBaseDateInDays(crfYearOneDateOffsets.getPulmonaryEvalDateOffset(i));
            e.setOffsetToLastAvailableDayInDays(crfYearOneDateOffsets.getPulmonaryEvalLastDateOffset(i));
            newEvents.add(e);
        }
        
        formTitle = "Quality of Life Year Two Form";
        e = new Event(null);
        e.setType(ECPEventTypes.QUALITY_OF_LIFE);
        e.setName("Quality of Life Year Two");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.QUALITY_OF_LIFE, formTitle));
        e.setExpected(true);
        e.setBaseDate(participant.getEnrolledDate());
        e.setOffsetFromBaseDateInDays(545);
        e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
        newEvents.add(e);
        
        return newEvents;
    }
    
    public static List<Event> getYearTwoEvents(Participant participant, boolean createCurrentTherapies) {
        List<Event> newEvents = new ArrayList<>();
        CRFYearTwoDateOffsets crfYearTwoDateOffsets = new CRFYearTwoDateOffsets();
        Event e;
        String formTitle;
        
        formTitle = "Annual Follow Up Year Three Log";
        e = new Event(null);
        e.setType(ECPEventTypes.ANNUAL_FOLLOW_UP);
        e.setName("Annual Follow Up Year Three Log");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.ANNUAL_FOLLOW_UP, formTitle));
        e.setExpected(true);
        e.setBaseDate(participant.getEnrolledDate());

        //set actual date
        Date enrollmentDate = participant.getStudyArmEnrollDate();    
        Calendar c = Calendar.getInstance();
        c.setTime(enrollmentDate);
        c.add(Calendar.DATE, 910);
        Date startDate = c.getTime();
        e.setActualDate(startDate);

        e.setOffsetFromBaseDateInDays(731);
        e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
        newEvents.add(e);
        
        if (createCurrentTherapies == true) {
            for(int i = 1; i < crfYearTwoDateOffsets.getCurrentTherapyDateOffsets().length; i++){
                formTitle = "Current Therapy " + crfYearTwoDateOffsets.getCurrentTherapyDateOffset(i) + " Form";
                e = new Event(null);
                e.setType(ECPEventTypes.CURRENT_THERAPY);
                e.setName("Current Therapy " + crfYearTwoDateOffsets.getCurrentTherapyName(i));
                e.setParticipantId(participant.getId());
                e.setStatus(EventStatus.NEW);
                e.setForms(ECPEvents.getForms(ECPEventTypes.CURRENT_THERAPY, formTitle));
                e.setExpected(true);
                e.setBaseDate(participant.getEnrolledDate());
                e.setOffsetFromBaseDateInDays(crfYearTwoDateOffsets.getCurrentTherapyDateOffset(i));
                e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
                newEvents.add(e);
            }
        }
        
        for(int i = 1; i < crfYearTwoDateOffsets.getPulmonaryEvalDateOffsets().length; i++){
            formTitle = "Pulmonary Evaluation: " + crfYearTwoDateOffsets.getPulmonaryEvalName(i) + " Form";
            e = new Event(null);
            e.setType(ECPEventTypes.PULMONARY_EVAL);
            e.setName("Pulmonary Evaluation: " + crfYearTwoDateOffsets.getPulmonaryEvalName(i));
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.PULMONARY_EVAL, formTitle));
            e.setExpected(true);
            e.setBaseDate(participant.getEnrolledDate());
            e.setOffsetFromBaseDateInDays(crfYearTwoDateOffsets.getPulmonaryEvalDateOffset(i));
            e.setOffsetToLastAvailableDayInDays(crfYearTwoDateOffsets.getPulmonaryEvalLastDateOffset(i));
            newEvents.add(e);
        }
        
        formTitle = "Quality of Life Year Three Form";
        e = new Event(null);
        e.setType(ECPEventTypes.QUALITY_OF_LIFE);
        e.setName("Quality of Life Year Three");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.QUALITY_OF_LIFE, formTitle));
        e.setExpected(true);
        e.setBaseDate(participant.getEnrolledDate());
        e.setOffsetFromBaseDateInDays(910);
        e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
        newEvents.add(e);
        
        return newEvents;
    }
    
    public static List<Event> getYearThreeEvents(Participant participant, boolean createCurrentTherapies) {
        List<Event> newEvents = new ArrayList<>();
        CRFYearThreeDateOffsets crfYearThreeDateOffsets = new CRFYearThreeDateOffsets();
        Event e;
        String formTitle;
 
        formTitle = "Annual Follow Up Year Four Log";
        e = new Event(null);
        e.setType(ECPEventTypes.ANNUAL_FOLLOW_UP);
        e.setName("Annual Follow Up Year Four Log");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.ANNUAL_FOLLOW_UP, formTitle));
        e.setExpected(true);
        e.setBaseDate(participant.getEnrolledDate());

        //set actual date
        Date enrollmentDate = participant.getStudyArmEnrollDate();    
        Calendar c = Calendar.getInstance();
        c.setTime(enrollmentDate);
        c.add(Calendar.DATE, 1275);
        Date startDate = c.getTime();
        e.setActualDate(startDate);

        e.setOffsetFromBaseDateInDays(1096);
        e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
        newEvents.add(e);
        
        if (createCurrentTherapies == true) {
            for(int i = 1; i < crfYearThreeDateOffsets.getCurrentTherapyDateOffsets().length; i++){
                formTitle = "Current Therapy " + crfYearThreeDateOffsets.getCurrentTherapyDateOffset(i) + " Form";
                e = new Event(null);
                e.setType(ECPEventTypes.CURRENT_THERAPY);
                e.setName("Current Therapy " + crfYearThreeDateOffsets.getCurrentTherapyName(i));
                e.setParticipantId(participant.getId());
                e.setStatus(EventStatus.NEW);
                e.setForms(ECPEvents.getForms(ECPEventTypes.CURRENT_THERAPY, formTitle));
                e.setExpected(true);
                e.setBaseDate(participant.getEnrolledDate());
                e.setOffsetFromBaseDateInDays(crfYearThreeDateOffsets.getCurrentTherapyDateOffset(i));
                e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
                newEvents.add(e);
            }
        }
        
        for(int i = 1; i < crfYearThreeDateOffsets.getPulmonaryEvalDateOffsets().length; i++){
            formTitle = "Pulmonary Evaluation: " + crfYearThreeDateOffsets.getPulmonaryEvalName(i) + " Form";
            e = new Event(null);
            e.setType(ECPEventTypes.PULMONARY_EVAL);
            e.setName("Pulmonary Evaluation: " + crfYearThreeDateOffsets.getPulmonaryEvalName(i));
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.PULMONARY_EVAL, formTitle));
            e.setExpected(true);
            e.setBaseDate(participant.getEnrolledDate());
            e.setOffsetFromBaseDateInDays(crfYearThreeDateOffsets.getPulmonaryEvalDateOffset(i));
            e.setOffsetToLastAvailableDayInDays(crfYearThreeDateOffsets.getPulmonaryEvalLastDateOffset(i));
            newEvents.add(e);
        }
        
        formTitle = "Quality of Life Year Four Form";
        e = new Event(null);
        e.setType(ECPEventTypes.QUALITY_OF_LIFE);
        e.setName("Quality of Life Year Four");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.QUALITY_OF_LIFE, formTitle));
        e.setExpected(true);
        e.setBaseDate(participant.getEnrolledDate());
        e.setOffsetFromBaseDateInDays(1275);
        e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
        newEvents.add(e);
        
        return newEvents;
    }
    
    public static List<Event> getYearFourEvents(Participant participant, boolean createCurrentTherapies) {
        List<Event> newEvents = new ArrayList<>();
        CRFYearFourDateOffsets crfYearFourDateOffsets = new CRFYearFourDateOffsets();
        Event e;
        String formTitle;
        
        formTitle = "Annual Follow Up Year Five Log";
        e = new Event(null);
        e.setType(ECPEventTypes.ANNUAL_FOLLOW_UP);
        e.setName("Annual Follow Up Year Five Log");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.ANNUAL_FOLLOW_UP, formTitle));
        e.setExpected(true);
        e.setBaseDate(participant.getEnrolledDate());

        //set actual date
        Date enrollmentDate = participant.getStudyArmEnrollDate();    
        Calendar c = Calendar.getInstance();
        c.setTime(enrollmentDate);
        c.add(Calendar.DATE, 1640);
        Date startDate = c.getTime();
        e.setActualDate(startDate);

        e.setOffsetFromBaseDateInDays(1461);
        e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
        newEvents.add(e);
        
        if (createCurrentTherapies == true) {
            for(int i = 1; i < crfYearFourDateOffsets.getCurrentTherapyDateOffsets().length; i++){
                formTitle = "Current Therapy " + crfYearFourDateOffsets.getCurrentTherapyDateOffset(i) + " Form";
                e = new Event(null);
                e.setType(ECPEventTypes.CURRENT_THERAPY);
                e.setName("Current Therapy " + crfYearFourDateOffsets.getCurrentTherapyName(i));
                e.setParticipantId(participant.getId());
                e.setStatus(EventStatus.NEW);
                e.setForms(ECPEvents.getForms(ECPEventTypes.CURRENT_THERAPY, formTitle));
                e.setExpected(true);
                e.setBaseDate(participant.getEnrolledDate());
                e.setOffsetFromBaseDateInDays(crfYearFourDateOffsets.getCurrentTherapyDateOffset(i));
                e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
                newEvents.add(e);
            }
        }
        
        for(int i = 1; i < crfYearFourDateOffsets.getPulmonaryEvalDateOffsets().length; i++){
            formTitle = "Pulmonary Evaluation: " + crfYearFourDateOffsets.getPulmonaryEvalName(i) + " Form";
            e = new Event(null);
            e.setType(ECPEventTypes.PULMONARY_EVAL);
            e.setName("Pulmonary Evaluation: " + crfYearFourDateOffsets.getPulmonaryEvalName(i));
            e.setParticipantId(participant.getId());
            e.setStatus(EventStatus.NEW);
            e.setForms(ECPEvents.getForms(ECPEventTypes.PULMONARY_EVAL, formTitle));
            e.setExpected(true);
            e.setBaseDate(participant.getEnrolledDate());
            e.setOffsetFromBaseDateInDays(crfYearFourDateOffsets.getPulmonaryEvalDateOffset(i));
            e.setOffsetToLastAvailableDayInDays(crfYearFourDateOffsets.getPulmonaryEvalLastDateOffset(i));
            newEvents.add(e);
        }
        
        formTitle = "Quality of Life Year Five Form";
        e = new Event(null);
        e.setType(ECPEventTypes.QUALITY_OF_LIFE);
        e.setName("Quality of Life Year Five");
        e.setParticipantId(participant.getId());
        e.setStatus(EventStatus.NEW);
        e.setForms(ECPEvents.getForms(ECPEventTypes.QUALITY_OF_LIFE, formTitle));
        e.setExpected(true);
        e.setBaseDate(participant.getEnrolledDate());
        e.setOffsetFromBaseDateInDays(1640);
        e.setShowLastAvailableDate(false);  // Turn off the showing of the date in the Participant Summary page under the Overdue Date column.
        newEvents.add(e);
        
        return newEvents;
    }
}
