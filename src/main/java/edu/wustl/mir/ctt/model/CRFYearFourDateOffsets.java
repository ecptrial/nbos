/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.wustl.mir.ctt.model;

/**
 *
 * @author Lauren Wallace
 */
public class CRFYearFourDateOffsets {
    private final int[] currentTherapyDateOffsets = {0, 1550, 1640, 1730, 1825};
    private final String[] currentTherapyNames = {"", "Year Five Day 90", "Year Five Day 180", "Year Five Day 270", "Year Five Day 365"};

    private final int[] pulmonaryEvalDateOffsets = {0, 1520, 1580, 1640, 1700, 1760, 1825};
    private final int[] pulmonaryEvalLastDateOffsets = {0, 15, 15, 15, 15, 15, 15};
    private final String[] pulmonaryEvalNames = {"", "Year Five Day 60", "Year Five Day 120", "Year Five Day 180", "Year Five Day 240", "Year Five Day 300", "Year Five Day 365"};
    
    public int[] getCurrentTherapyDateOffsets() {
        return currentTherapyDateOffsets;
    }
    
    public int getCurrentTherapyDateOffset(int i) {
        return currentTherapyDateOffsets[i];
    }
    
    public String getCurrentTherapyName(int i) {
        return currentTherapyNames[i];
    }

    public int[] getPulmonaryEvalDateOffsets() {
        return pulmonaryEvalDateOffsets;
    }
    
    public int[] getPulmonaryEvalLastDateOffsets() {
        return pulmonaryEvalLastDateOffsets;
    }
    
    public int getPulmonaryEvalDateOffset(int i) {
        return pulmonaryEvalDateOffsets[i];
    }
    
    public int getPulmonaryEvalLastDateOffset(int i) {
        return pulmonaryEvalLastDateOffsets[i];
    }
    
    public String getPulmonaryEvalName(int i) {
        return pulmonaryEvalNames[i];
    }
}
