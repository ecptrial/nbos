package edu.wustl.mir.ctt;

import edu.wustl.mir.ctt.model.Site;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.ReportPersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import edu.wustl.mir.ctt.reports.ReportItem;
import edu.wustl.mir.ctt.reports.ReportItems;
import edu.wustl.mir.ctt.log.AuditLogger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.servlet.http.HttpServletResponse;


@ManagedBean
@SessionScoped
public class ReportsController implements Serializable {

    private ReportItems reportItemsForEpiArm;
    private ReportItems reportItemsForControlArm;
    private ReportItems reportItemsForCurrentCRFStatus;
    // The dataModel cannot be serialized and saved to a hard drive because a NotSerializableException error occurs, 
    // so the dataModel is saved in the ReportItems.java code. The ReportItems.java code is not serialized.
    private transient DataModel dataModelEpiArm;  // The transient causes the dataModelEpi to not be stored so you do not get a NotSerializableException error.
                                                        // But you need to do lazy loading for the getDataModelEpi() method below.
    private transient DataModel dataModelControlArm;  // The transient causes the dataModelEcpControl to not be stored so you do not get a NotSerializableException error.
                                                        // But you need to do lazy loading for the getDataModelEcpControl() method below.
    private transient DataModel dataModelCurrentCRFStatus;
    
    private ArrayList<String> columnNames;

    private AuditLogger logger;

    private String crfOutcome;


    private Date crfInputDate;
    // Constructor
    public ReportsController() {
        logger = AuditLogger.create(ReportsController.class);
        crfInputDate = getCurrentDate();
        crfOutcome = "";
    }

    public String getCrfOutcome() {
        return crfOutcome;
    }

    public void setCrfOutcome(String crfOutcome) {
        this.crfOutcome = crfOutcome;
    }

    public Date getCrfInputDate() {
        return crfInputDate;
    }

    public void setCrfInputDate(Date crfInputDate) {
        this.crfInputDate = crfInputDate;
    }

    public ReportItems getReportItemsForEpiArm(){
        return reportItemsForEpiArm;
    }
    
    public ReportItems getReportItemsForControlArm(){
        return reportItemsForControlArm;
    }
    
    public ReportItems getReportItemsForCurrentCRFStatus(){
        return reportItemsForCurrentCRFStatus;
    }
    
    public String keyContacts() {
        return "/keyContacts.xhtml?faces-redirect=true";
    }
    
    public String supportStaff() {
        return "/supportStaff.xhtml?faces-redirect=true";
    }
/*    
    public void viewEvent( ActionEvent ae) {
        String eventId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectedEventId");
        System.out.println( eventId);
    }
*/    
    public String editSiteAction() {
        return "/editSite.xhtml?faces-redirect=true";
    }
    
    public String clearFormAction() {
        return null;
    }
    
      
    public String homePage(){
        System.out.println("The home page button was selected!!");
        return "main.xhtml?faces-redirect=true";
    }
    
    public Date getCurrentDate(){
        Date currentDate = new Date();
        return currentDate;
    }
    
    // The getDataModelEpiArm() method is used by the monthlyAccrualReport.xhtml code to dynamically display varying numbers of columns in the table.
    public DataModel getDataModelEpiArm() throws PersistenceException {
        // The following three lines of code are needed for lazy loading if the dataModelEpi is transient.
        // Otherwise, if the dataModel is not transient, comment out these three lines.
        if(this.dataModelEpiArm == null){
//            this.dataModel = this.getAccrualBySiteByMonthReportItems();  // This line of code works also, but it runs the getAccrualBySiteByMonthReportItems() again which is not necessary.
            this.dataModelEpiArm = this.getReportItemsForEpiArm().getDataModel();
        }
            return dataModelEpiArm;
    }

    public void setDataModelEpiArm(DataModel dataModelEpiArm){
        this.dataModelEpiArm = dataModelEpiArm;
    }

    // The getDataModelControlArm() method is used by the monthlyAccrualReport.xhtml code to dynamically display varying numbers of columns in the table.
    public DataModel getDataModelControlArm() throws PersistenceException {
        // The following three lines of code are needed for lazy loading if the dataModelEcpControl is transient.
        // Otherwise, if the dataModel is not transient, comment out these three lines.
        if(this.dataModelControlArm == null){
//            this.dataModel = this.getAccrualBySiteByMonthReportItems();  // This line of code works also, but it runs the getAccrualBySiteByMonthReportItems() again which is not necessary.
            this.dataModelControlArm = this.getReportItemsForControlArm().getDataModel();
        }
            return dataModelControlArm;
    }
    
    // The getDataModelCurrentCRFStatus method for the CRF Status Report
    public DataModel getDataModelCurrentCRFStatus() throws PersistenceException {
        if(this.dataModelCurrentCRFStatus == null){
            this.dataModelCurrentCRFStatus = this.getReportItemsForCurrentCRFStatus().getDataModel();
        }
            return dataModelCurrentCRFStatus;
    }
    
    public void setDataModelEcpControl(DataModel dataModelEcpControl){
        this.dataModelControlArm = dataModelEcpControl;
    }

    public ArrayList<String> getColumnNames() {
            return columnNames;
    }

    public void setColumnNames(ArrayList<String> columnNames) {
            this.columnNames = columnNames;
    }


    /**
     * Indicate if the ECP Report Menu items can be rendered if you are a technical coordinator.
     * 
     * True if you are logged in as a technical coordinator.
     * 
     * @return 
     */
    public boolean renderECPReportMenuItems() {
        return SecurityManager.canVerifyForms();
    }

    public String MonthlySiteCRFStatusReportAction() throws PersistenceException {
        this.createAccrualBySiteByMonthReportItems();
        return "/monthlySiteCRFStatusReport.xhtml?faces-redirect=true";
    }

    // Return the newly created totalAccuralReport.xhtml to the browswer for display. 
    // NOTE: the data within the totalAccrualReport.xhtml is created by each method call within the xhtml code.
    public String TotalAccrualReportAction() throws PersistenceException {
        // Remove the comment from the get method below if you want to display Table 4 in the totalAccrualReport.xhtml code.
        // The following method is used to show how the dataModel and columnNames are created from the raw database resultset for use in the Primefaces
        // dynamic column names dataTable code.
//        this.dataModel = this.getAccrualBySiteByMonthRawDatabaseRetrievalReportItems();
//// The following this.createParticipantFirstPaymentStatus method was added to start testing the First Payment Status software.
////        this.createParticipantFirstPaymentStatus();
        return "/totalAccrualReport.xhtml?faces-redirect=true";
    }
    
    // The MonthlyAccrualReportAction() method is called from the template.xhtml sub-menu "Monthly Accrual Report" under the main menu "ECP Reports".
    // Return the newly created monthlyAccuralReport.xhtml to the browswer for display, but the dataModelEpi, dataModelControl and columnNames need to be created first. 
    // The dataModel and columnNames used in the monthlyAccrualReport.xhtml are created by the createAccrualBySiteByMonthReportItems() method.
    public String MonthlyAccrualReportAction() throws PersistenceException {
        this.createAccrualBySiteByMonthReportItems();
        return "/monthlyAccrualReport.xhtml?faces-redirect=true";
    }
    
    //Methods for Current CRF Status Report
    public String CurrentCRFStatusReportAction() throws PersistenceException {
        this.createCurrentCRFStatusReportItems();
        return "/currentCRFStatusReport.xhtml?faces-redirect=true";
    }
    
    public void createCurrentCRFStatusReportItems() throws PersistenceException {
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();  // Get the ReportPersistenceManager.
        // Use the ReportPersistenceManager getCurrentCRFStatus() method to create the dataModel and columnNames for the Primefaces dataTable.
        ReportItems reportItems = rpm.getCurrentCRFStatus();
        // Save the dataModel and columnNames locally after creation.
        this.dataModelCurrentCRFStatus = reportItems.getDataModel();
        this.columnNames = reportItems.getColumnNames();
    }
    
    public List<ReportItem> getAccrualReportItems() throws PersistenceException {
        List<ReportItem> totalAccruals = new ArrayList<>(5);
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        totalAccruals = rpm.getTotalAccruals();

        return totalAccruals;
    }
    
    public List<ReportItem> getAccrualLast30DaysReportItems() throws PersistenceException {
        List<ReportItem> totalAccrualsLast30Days = new ArrayList<>(5);
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        totalAccrualsLast30Days = rpm.getTotalAccrualsLast30Days();

        return totalAccrualsLast30Days;
    }
    
    public List<ReportItem> getAccrualBySiteReportItems() throws PersistenceException {
        List<ReportItem> totalAccrualsBySite = new ArrayList<>(5);
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        totalAccrualsBySite = rpm.getTotalAccrualsBySite();

        return totalAccrualsBySite;
    }
    
    // Create the dataModelEpi and columNames and save them here in the ReportsController to be called by the totalAccrualReport.xhtml Primefaces Table 4 making code. 
    // Table 4 is currently commented out in the totalAccrualReport.xhtml code.
    public DataModel getAccrualBySiteByMonthRawDatabaseRetrievalReportItems() throws PersistenceException {
        ReportItems reportItems = new ReportItems();
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        reportItems = rpm.getTotalAccrualsBySiteByMonthRawDatabaseRetrieval();
        this.dataModelEpiArm = reportItems.getDataModel();
        this.columnNames = reportItems.getColumnNames();
        return reportItems.getDataModel();
    }

    // The createAccrualBySiteByMonthReportItems method is used to create the dataModelEpi and columNames.
    // It saves both of them here in the ReportsController to be called by the monthlyAccrualReport.xhtml Primefaces table making code. 
    public void createAccrualBySiteByMonthReportItems() throws PersistenceException {
//        ReportItems reportItems = new ReportItems();
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();  // Get the ReportPersistenceManager.
        // Use the ReportPersistenceManager getTotalAccrualsBySiteByMonth() method to create the dataModel and columnNames for the Primefaces dataTable.
        ReportItems reportItems = rpm.getTotalAccrualsBySiteByMonth("epi_arm");
        // Save the dataModelEpi and columnNames locally after creation.
        this.dataModelEpiArm = reportItems.getDataModel();
        this.columnNames = reportItems.getColumnNames();

        // Use the ReportPersistenceManager getTotalAccrualsBySiteByMonth() method to create the dataModel and columnNames for the Primefaces dataTable.
        reportItems = rpm.getTotalAccrualsBySiteByMonth("control_arm");
        // Save the dataModelEcpControl locally after creation.
        this.dataModelControlArm = reportItems.getDataModel();

    }

    public String createParticipantFirstPaymentStatus() throws PersistenceException {
        
        Integer formCount = 0;
        int siteid;
        String pid;
        String eventName;
        
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        
        siteid = 2;
        pid = "101001";
        eventName = "Confirmation of Eligibility";
        
        formCount = rpm.getParticipantFirstPaymentStatus(siteid, pid, eventName);
        System.out.println("The form count for event named " + eventName + " is: " + formCount + "\n");

        eventName = "ECP Treatment 1";
        
        formCount = rpm.getParticipantFirstPaymentStatus(siteid, pid, eventName);
        System.out.println("The form count for event named " + eventName + " is: " + formCount + "\n");

        return formCount.toString();
    }

    public void copyFilesAction() throws PersistenceException, IOException {
        String date = convertDateToValidString(this.crfInputDate, "yyyy-MM-dd");
        
        Calendar c =  Calendar.getInstance();
        c.setTime(this.crfInputDate);
        c.add(c.DATE,-1);
        
        String month = getMonth(c.getTime());
        String timestamp = new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date()) + "NBOSReport";
        String dayBefore = getDayBefore(this.crfInputDate);
        try {
            ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
            List<Site> sites = rpm.getSites();
            HashMap<String, Integer> nameToId = new HashMap<>();

            for (Site site : sites) {
                if (site.getId() > 1) {
                    nameToId.put(site.getName() + "" + month + "CRFReportNewBOS" + timestamp, site.getSiteID());
                }
            }

            //If the MonthlySiteReportsRBOS directory doesn't already exist, this snippet creates a new directory.
            if (!Files.exists(Paths.get("/home/shared/WUSTL/ecp/nbos/MonthlySiteReportsNewBOS"))) {
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-x---"));

                Files.createDirectory(Paths.get("/home/shared/WUSTL/ecp/nbos/MonthlySiteReportsNewBOS"), fa);


            }

            if (!Files.exists(Paths.get("/home/shared/WUSTL/ecp/nbos/MonthlySiteReportsNewBOS/" + timestamp))) {
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-x---"));

                Files.createDirectory(Paths.get("/home/shared/WUSTL/ecp/nbos/MonthlySiteReportsNewBOS/" + timestamp), fa);


            }

            for (String name : nameToId.keySet()) {
                File csv = new File("/home/shared/WUSTL/ecp/nbos/MonthlySiteReportsNewBOS/" + timestamp + "/" + name + ".xlsx");
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rw-r-----"));
                Files.createFile(Paths.get("/home/shared/WUSTL/ecp/nbos/MonthlySiteReportsNewBOS/" + timestamp + "/" + name + ".xlsx"), fa);

            }
            for (String name : nameToId.keySet()) {
                rpm.runCRFCopy(name, nameToId.get(name), date, dayBefore, timestamp);
            }

            zipDirectory(nameToId.keySet(), timestamp);

            downloadZip(new File("/home/shared/WUSTL/ecp/nbos/MonthlySiteReportsNewBOS/" + timestamp + ".zip"));

            //emailZip(timestamp); No longer used due to security reasons (must encrypt email)
            try {
                Thread.sleep(3000); // Letting the nsf files catch up to the closed streams so it can delete its lock files
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            cleanTMP(timestamp);
            this.crfOutcome = "Download completed successfully";
        }catch(PersistenceException e){
            logger.error("PersistenceException", e);

        }catch(IOException ex) {
            logger.error("IOException", ex);
        }
    }

    public void cleanTMP(String timestamp){
        File f = new File("/home/shared/WUSTL/ecp/nbos/MonthlySiteReportsNewBOS/"+timestamp);
        File[] files = f.listFiles();

        for (File file: files){
            file.delete();
        }
        f.delete();

        File zip = new File("/home/shared/WUSTL/ecp/nbos/MonthlySiteReportsNewBOS/"+timestamp+".zip");
        zip.delete();
    }

    public void zipDirectory(Set<String> files, String timestamp) {
        ZipOutputStream zos = null;
        BufferedInputStream bis = null;

        try {
            logger.audit("files" + files.toString());
            zos = new ZipOutputStream(new FileOutputStream("/home/shared/WUSTL/ecp/nbos/MonthlySiteReportsNewBOS/"+timestamp+".zip"));

            for (String name : files) {

                ZipEntry ze = new ZipEntry(name + ".xlsx");
                zos.putNextEntry(ze);
                File f = new File("/home/shared/WUSTL/ecp/nbos/MonthlySiteReportsNewBOS/"+timestamp+"/" + name +".xlsx");

                bis = new BufferedInputStream(new FileInputStream(
                        f));

                long bytesRead = 0;
                byte[] bytesIn = new byte[4096];
                int read = 0;
                while ((read = bis.read(bytesIn)) != -1) {
                    zos.write(bytesIn, 0, read);
                    bytesRead += read;
                }

                zos.closeEntry();
                bis.close();
                logger.audit("zos" + zos.toString());

            }

        }catch(IOException ex){
            logger.error("IOException", ex);
        }finally {
            try {
                if (Objects.nonNull(zos)) {
                    zos.close();
                }
                if (Objects.nonNull(bis)) {
                    bis.close();
                }
            }catch(IOException e){
                logger.error("IOException", e);
            }
        }

    }

    public void downloadZip(File f){
        OutputStream ros = null;
        FileInputStream fis = null;
        try{
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
            String fileType = "application/zip";

            response.reset();
            response.setContentType(fileType);

            response.setHeader("Content-Disposition","attachment; filename="+f.getName());

            fis = new FileInputStream(f);
            ros = response.getOutputStream();

            long bytesRead = 0;
            byte[] bytesIn = new byte[4096];
            int c;
            while ((c = fis.read(bytesIn)) != -1){
                ros.write(bytesIn, 0, c);
                bytesRead += c;
            }

            ros.flush();
            ros.close();

            fis.close();

            fc.responseComplete();

        }catch (IOException e){
            logger.error("IOException", e);
        }
        finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (ros != null) {
                    ros.close();
                }
            }catch(IOException e){
                logger.error("IOException", e);
            }
        }


    }

    //Converts Date object into String needed in queries
    public String convertDateToValidString(Date date, String pattern){
        DateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    public String getMonth(Date date){
        String pattern = "MMMMM";
        DateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    public String getDayBefore(Date date){
        Calendar c =  Calendar.getInstance();
        c.setTime(date);
        c.add(c.DATE,-1);
        return convertDateToValidString(c.getTime(), "yyyy-MM-dd");

    }

}
