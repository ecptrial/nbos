package edu.wustl.mir.ctt.workbook;

import org.apache.commons.lang3.math.NumberUtils;
import edu.wustl.mir.ctt.log.AuditLogger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;

public class ResultSetWorkBook {
    private Workbook wb;
    private ResultSet rs;
    private ArrayList<String> cols;
    private Sheet sheet;
    private AuditLogger logger;
    private String path;
    private boolean published;
    public ResultSetWorkBook(ResultSet rs, String path) {
        logger = AuditLogger.create(ResultSetWorkBook.class);
        this.wb = new XSSFWorkbook();
        this.rs = rs;
        this.path = path;
        this.cols = new ArrayList<>();
        this.sheet = wb.createSheet("Monthly Report");
        this.published = false;
        this.generateCols();
    }

    public Workbook getWb() {
        return wb;
    }

    public void setWb(Workbook wb) {
        this.wb = wb;
    }

    public ResultSet getRs() {
        return rs;
    }

    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    public void generateCols(){
        if(!this.published)
        {
            try {
                ResultSetMetaData rsmd = rs.getMetaData();

                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    this.cols.add(rsmd.getColumnLabel(i));
                }

                Row header = this.sheet.createRow(0);

                for (int i = 0; i < this.cols.size(); i++) {
                    header.createCell(i).setCellValue(cols.get(i));
                }

            } catch (SQLException e) {
                logger.error("SQLException", e);
            }
        }
        else{
            logger.audit("Workbook already closed and published.");
        }
    }

    public void fillRows(){
        if(!this.published){
            try {
                int rowIndex = 0;
                while (this.rs.next()) {
                    Row row = sheet.createRow(++rowIndex); //Row 0 has the column names
                    for (int i = 0; i < cols.size(); i++){
                        Cell cell = row.createCell(i);
                        if (NumberUtils.isCreatable(Objects.toString(rs.getObject(cols.get(i)), ""))){
                            Integer value =  Integer.parseInt(Objects.toString(rs.getObject(cols.get(i))));
                            cell.setCellValue(value);

                        }else{
                            String value = Objects.toString(rs.getObject(cols.get(i)), "");
                            cell.setCellValue(value);
                        }
                    }
                }

            }catch (SQLException e){
                logger.error("SQLException", e);
            }
        }
    }

    public void publishFile(){
        if(!this.published)
        {
            FileOutputStream fos = null;
            try {
                for (int i = 0; i < cols.size(); i++){
                    sheet.autoSizeColumn(i);
                }

                fos = new FileOutputStream(path);
                wb.write(fos);
                this.published = true;
            } catch (FileNotFoundException e) {
                logger.error("FileNotFoundException", e);
            } catch (IOException e) {
                logger.error("IOException", e);
            }
            finally {
                try {
                    if (fos != null) {
                        fos.flush();
                        fos.close();
                    }
                    wb.close();
                }catch (IOException e) {
                    logger.error("IOException", e);
                }
            }
        }
        else{
            logger.audit("Workbook already closed and published.");
        }
    }
    @Override
    public String toString() {
        return "ResultSetWorkBook{" +
                "wb=" + wb +
                ", rs=" + rs +
                '}';
    }
}
