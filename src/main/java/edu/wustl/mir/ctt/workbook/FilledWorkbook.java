package edu.wustl.mir.ctt.workbook;

import edu.wustl.mir.ctt.ReportsController;
import edu.wustl.mir.ctt.log.AuditLogger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class FilledWorkbook {
    private Workbook wb;
    private ArrayList<String> cols;
    private int colsLen;
    private Sheet sheet;
    private AuditLogger logger;
    private boolean published;
    private String preset;

    public FilledWorkbook(String preset) {
        logger = AuditLogger.create(FilledWorkbook.class);
        this.wb = new XSSFWorkbook();
        this.cols = new ArrayList<>();
        this.colsLen = 0;
        this.preset = preset;
        this.published = false;

        switch(preset){
            case "monthlySiteCRFStatus":
                this.sheet = wb.createSheet("Monthly Site CRF Status Report");
                break;
            default:
                logger.audit("No preset found!");
        }
    }


    public void generateMonthlySiteCRFStatus(LinkedList<Object[]> rows) {
        if (!this.published && preset.equals("monthlySiteCRFStatus")) {
            colsLen = 7;
            Row header = this.sheet.createRow(0);
            header.createCell(0).setCellValue("Site Name");
            header.createCell(1).setCellValue("Participant ID");
            header.createCell(2).setCellValue("Enrolled Date");
            header.createCell(3).setCellValue("Event Name");
            header.createCell(4).setCellValue("Event Status");
            header.createCell(5).setCellValue("Projected Date");
            header.createCell(6).setCellValue("Days From Event");

            int rowIter = 1;
            ReportsController rc = new ReportsController();
            for (Object[] row: rows){
                header = this.sheet.createRow(rowIter++);

                header.createCell(0).setCellValue(Objects.toString(row[0]));
                header.createCell(1).setCellValue(Integer.parseInt(Objects.toString(row[1])));
                header.createCell(2).setCellValue(Objects.toString(rc.convertDateToValidString((Date) row[2], "MM/dd/yyyy")));
                header.createCell(3).setCellValue(Objects.toString(row[3]));
                header.createCell(4).setCellValue(Objects.toString(row[4]));
                header.createCell(5).setCellValue(Objects.toString(rc.convertDateToValidString((Date) row[5],"MM/dd/yyyy" )));
                header.createCell(6).setCellValue(Integer.parseInt(Objects.toString(row[6])));

            }


        } else {
            logger.audit("Workbook already closed and published. Or this is the wrong method for your preset");
        }
    }
    public void publishFile(String path){
        if(!this.published)
        {
            try {
                for (int i = 0; i < colsLen; i++){
                    sheet.autoSizeColumn(i);
                }

                FileOutputStream fos = new FileOutputStream(path);
                wb.write(fos);
                fos.flush();
                fos.close();
                wb.close();
                this.published = true;
            } catch (FileNotFoundException e) {
                logger.error("FileNotFoundException", e);
            } catch (IOException e) {
                logger.error("IOException", e);
            }
        }
        else{
            logger.audit("Workbook already closed and published.");
        }
    }
}

