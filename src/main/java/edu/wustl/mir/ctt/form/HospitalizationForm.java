package edu.wustl.mir.ctt.form;

import edu.wustl.mir.ctt.model.AttributeDate;
import edu.wustl.mir.ctt.model.AttributeFloat;
import edu.wustl.mir.ctt.model.AttributeInteger;
import edu.wustl.mir.ctt.model.AttributeString;
import edu.wustl.mir.ctt.model.ECPFormTypes;
import edu.wustl.mir.ctt.model.VerificationStatus;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Paul K. Commean
 */
public class HospitalizationForm extends BasicForm implements Serializable {
    
    public static final String[] SourceDocumentTypes = new String[]{"Admission Progress Note / Emergency Department Note", "Acute Ambulatory Care Note / Acute Clinic Note", "Discharge Summary"};
    
    public HospitalizationForm() {
        // constructor
        super();
        formType = ECPFormTypes.HOSPITALIZATION;
        title = "Hospitalization Form";
        this.sourceDocumentTypes = SourceDocumentTypes;

        attributes.put("admissionEDDate", new AttributeDate("admissionEDDate", true, false, true));        
        attributes.put("admissionEDDiagnosis", new AttributeString("admissionEDDiagnosis", true, false, true));
        attributes.put("dischargeDate", new AttributeDate("dischargeDate", true, false, true));        
        attributes.put("dischargeDiagnosis", new AttributeString("dischargeDiagnosis", true, false, true));
        attributes.put("comment", new AttributeString("comment"));
        
        //Lauren's note 7/12/21: a number of form attributes moved to the DSC form
        //were still being created here, I have removed them and their associated get/set methods
        
        this.clear();
    }
    
    public HospitalizationForm( BasicForm bf) {
        super(bf);		
        formType = ECPFormTypes.HOSPITALIZATION;
        title = bf.getTitle();
        this.sourceDocumentTypes = SourceDocumentTypes;
    }
	
    public Date getAdmissionEDDate() {
        return (Date) attributes.get("admissionEDDate").getValue();
    }
    
    public void setAdmissionEDDate(Date admissionEDDate) {
        attributes.get("admissionEDDate").setValue(admissionEDDate);
    }

    public VerificationStatus getAdmissionEDDateVerificationStatus() {
        return attributes.get("admissionEDDate").getVerificationStatus();
    }
    
    public void setAdmissionEDDateVerificationStatus(VerificationStatus verificationStatus) {
        attributes.get("admissionEDDate").setVerificationStatus(verificationStatus);
    }

    public String getAdmissionEDDateDccComment() {
        return (String) attributes.get("admissionEDDate").getDccComment();
    }
    
    public void setAdmissionEDDateDccComment(String dccComment) {
        attributes.get("admissionEDDate").setDccComment(dccComment);
    }


    public String getAdmissionEDDiagnosis() {
        return (String) attributes.get("admissionEDDiagnosis").getValue();
    }
    
    public void setAdmissionEDDiagnosis(String admissionEDDiagnosis) {
        attributes.get("admissionEDDiagnosis").setValue(admissionEDDiagnosis);
    }
    
    public VerificationStatus getAdmissionEDDiagnosisVerificationStatus() {
        return attributes.get("admissionEDDiagnosis").getVerificationStatus();
    }
    
    public void setAdmissionEDDiagnosisVerificationStatus(VerificationStatus verificationStatus) {
        attributes.get("admissionEDDiagnosis").setVerificationStatus(verificationStatus);
    }

    public String getAdmissionEDDiagnosisDccComment() {
        return (String) attributes.get("admissionEDDiagnosis").getDccComment();
    }
    
    public void setAdmissionEDDiagnosisDccComment(String dccComment) {
        attributes.get("admissionEDDiagnosis").setDccComment(dccComment);
    }


    public Date getDischargeDate() {
        return (Date) attributes.get("dischargeDate").getValue();
    }
    
    public void setDischargeDate(Date dischargeDate) {
        attributes.get("dischargeDate").setValue(dischargeDate);
    }

    public VerificationStatus getDischargeDateVerificationStatus() {
        return attributes.get("dischargeDate").getVerificationStatus();
    }
    
    public void setDischargeDateVerificationStatus(VerificationStatus verificationStatus) {
        attributes.get("dischargeDate").setVerificationStatus(verificationStatus);
    }

    public String getDischargeDateDccComment() {
        return (String) attributes.get("dischargeDate").getDccComment();
    }
    
    public void setDischargeDateDccComment(String dccComment) {
        attributes.get("dischargeDate").setDccComment(dccComment);
    }


    public String getDischargeDiagnosis() {
        return (String) attributes.get("dischargeDiagnosis").getValue();
    }
    
    public void setDischargeDiagnosis(String dischargeDiagnosis) {
        attributes.get("dischargeDiagnosis").setValue(dischargeDiagnosis);
    }
    
    public VerificationStatus getDischargeDiagnosisVerificationStatus() {
        return attributes.get("dischargeDiagnosis").getVerificationStatus();
    }
    
    public void setDischargeDiagnosisVerificationStatus(VerificationStatus verificationStatus) {
        attributes.get("dischargeDiagnosis").setVerificationStatus(verificationStatus);
    }

    public String getDischargeDiagnosisDccComment() {
        return (String) attributes.get("dischargeDiagnosis").getDccComment();
    }
    
    public void setDischargeDiagnosisDccComment(String dccComment) {
        attributes.get("dischargeDiagnosis").setDccComment(dccComment);
    }

    public String getComment() {
        return (String) attributes.get("comment").getValue();
    }
    
    public void setComment(String comment) {
        attributes.get("comment").setValue(comment);
    }
}

