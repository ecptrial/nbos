package edu.wustl.mir.ctt.form;

import edu.wustl.mir.ctt.model.Attribute;
import edu.wustl.mir.ctt.model.AttributeDate;
import edu.wustl.mir.ctt.model.AttributeInteger;
import edu.wustl.mir.ctt.model.AttributeString;
import edu.wustl.mir.ctt.model.ECPFormTypes;
import edu.wustl.mir.ctt.model.VerificationStatus;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.PersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import edu.wustl.mir.ctt.log.AuditLogger;


/**
 *
 * @author pkc
 */
public class AnnualFollowUpForm extends BasicForm {
    private transient AuditLogger logger = AuditLogger.create(AnnualFollowUpForm.class);

    public static final String[] SourceDocumentTypes = new String[]{"ECP Treatment Source Packet"};
    private List<Date> followUpECPTxDates;

    public AnnualFollowUpForm() {
        // constructor
        super();
        formType = ECPFormTypes.ANNUAL_FOLLOW_UP;
        title = "Annual Follow-Up Form";
        this.sourceDocumentTypes = SourceDocumentTypes;

        attributes.put("annualFollowUpDate", new AttributeDate("annualFollowUpDate"));        
        attributes.put("annualFollowUpReceivingECPTx", new AttributeString("annualFollowUpReceivingECPTx"));
        attributes.put("annualFollowUpECPTxDate1", new AttributeDate("annualFollowUpECPTxDate1", true, false, true));
        attributes.put("annualFollowUpECPTxDate2", new AttributeDate("annualFollowUpECPTxDate2"));
        attributes.put("annualFollowUpECPTxDate3", new AttributeDate("annualFollowUpECPTxDate3"));
        attributes.put("annualFollowUpECPTxDate4", new AttributeDate("annualFollowUpECPTxDate4"));
        attributes.put("annualFollowUpECPTxDate5", new AttributeDate("annualFollowUpECPTxDate5"));
        attributes.put("annualFollowUpECPTxDate6", new AttributeDate("annualFollowUpECPTxDate6"));

        initFollowUpTxDates();

        this.clear();
    }
    
    public AnnualFollowUpForm( BasicForm bf) {
        super(bf);		
        title = bf.getTitle();
        this.sourceDocumentTypes = SourceDocumentTypes;
        initFollowUpTxDates();
    }
	
    public Date getAnnualFollowUpDate() {
        return (Date) attributes.get("annualFollowUpDate").getValue();
    }
    
    public void setAnnualFollowUpDate(Date annualFollowUpDate) {
        attributes.get("annualFollowUpDate").setValue(annualFollowUpDate);
    }

    
    public String getAnnualFollowUpReceivingECPTx() {
        return (String) attributes.get("annualFollowUpReceivingECPTx").getValue();
    }
    
    public void setAnnualFollowUpReceivingECPTx(String annualFollowUpReceivingECPTx) {
        attributes.get("annualFollowUpReceivingECPTx").setValue(annualFollowUpReceivingECPTx);
    }
    
    public Date getAnnualFollowUpECPTxDate1() {
        return (Date) attributes.get("annualFollowUpECPTxDate1").getValue();
    }
    
    public void setAnnualFollowUpECPTxDate1(Date annualFollowUpECPTxDate1) {
        attributes.get("annualFollowUpECPTxDate1").setValue(annualFollowUpECPTxDate1);
    }

    public Date getAnnualFollowUpECPTxDate2() {
        return (Date) attributes.get("annualFollowUpECPTxDate2").getValue();
    }
    
    public void setAnnualFollowUpECPTxDate2(Date annualFollowUpECPTxDate2) {
        attributes.get("annualFollowUpECPTxDate2").setValue(annualFollowUpECPTxDate2);
    }

    public Date getAnnualFollowUpECPTxDate3() {
        return (Date) attributes.get("annualFollowUpECPTxDate3").getValue();
    }
    
    public void setAnnualFollowUpECPTxDate3(Date annualFollowUpECPTxDate3) {
        attributes.get("annualFollowUpECPTxDate3").setValue(annualFollowUpECPTxDate3);
    }

    public Date getAnnualFollowUpECPTxDate4() {
        return (Date) attributes.get("annualFollowUpECPTxDate3").getValue();
    }
    
    public void setAnnualFollowUpECPTxDate4(Date annualFollowUpECPTxDate4) {
        attributes.get("annualFollowUpECPTxDate3").setValue(annualFollowUpECPTxDate4);
    }
    
    public Date getAnnualFollowUpECPTxDate5() {
        return (Date) attributes.get("annualFollowUpECPTxDate3").getValue();
    }
    
    public void setAnnualFollowUpECPTxDate5(Date annualFollowUpECPTxDate5) {
        attributes.get("annualFollowUpECPTxDate3").setValue(annualFollowUpECPTxDate5);
    }
    
    public Date getAnnualFollowUpECPTxDate6() {
        return (Date) attributes.get("annualFollowUpECPTxDate3").getValue();
    }
    
    public void setAnnualFollowUpECPTxDate6(Date annualFollowUpECPTxDate6) {
        attributes.get("annualFollowUpECPTxDate3").setValue(annualFollowUpECPTxDate6);
    }
    public VerificationStatus getAnnualFollowUpECPTxDate1VerificationStatus() {
        logger.audit("AnnualFollowUpForm, getAnnualFollowUpECPTxDate1VerificationStatus");
        logger.audit("AnnualFollowUpForm, getAnnualFollowUpECPTxDate1VerificationStatus" + attributes.get("annualFollowUpECPTxDate1").getVerificationStatus());

//        System.out.println("getTerminationReasonVerify was called containing: " + attributes.get("terminationReason").getVerificationStatus());
        return attributes.get("annualFollowUpECPTxDate1").getVerificationStatus();
    }
    
    public void setAnnualFollowUpECPTxDate1VerificationStatus(VerificationStatus verificationStatus) {
//        System.out.println("SET TerminationReasonVerify was called containing: " + verificationStatus);
        attributes.get("annualFollowUpECPTxDate1").setVerificationStatus(verificationStatus);
    }

    public String getAnnualFollowUpECPTxDate1DccComment() {
        return (String) attributes.get("annualFollowUpECPTxDate1").getDccComment();
    }
    
    public void setAnnualFollowUpECPTxDate1DccComment(String dccComment) {
        attributes.get("annualFollowUpECPTxDate1").setDccComment(dccComment);
    }

    public List<Date> getFollowUpTxDates() {
        return followUpECPTxDates;
    }

    public void initFollowUpTxDates() {
        String dateKey = "annualFollowUpECPTxDate";

        List<String> keys = new ArrayList<>(attributes.keySet());
        
        // Remove other attributes to count current number of TxDates
        List<String> keysToRemove = new ArrayList<>();
        
        for (String k : keys) {
            if (!k.startsWith(dateKey))
                keysToRemove.add(k);
        }
        
        logger.audit("Found unneeded keys: " + keysToRemove.toString());
        
        keys.removeAll(keysToRemove);
        
        logger.audit("Remaining keys: " + keys.toString());
        
        // Copy all of the attributes into a list of dates
        List<Date> txDates = new ArrayList<>();
        Date d;
        
        for(int i = 0; i < keys.size(); i++){
            String attribute = dateKey + (i + 1);
            logger.audit("Processing " + attribute);
            
            d = (Date) attributes.get(attribute).getValue();
            txDates.add(d);
        }
        
        followUpECPTxDates = txDates;
    }
    
    public void saveFollowUpTxDates() {
        String dateKey = "annualFollowUpECPTxDate";
        
        // Sync list of dates back into attributes, creating attributes if they don't exist
        for (int i = 0; i < followUpECPTxDates.size(); ++i) {
            String attribute = dateKey + (i + 1);
            
            if (!attributes.containsKey(attribute)) {
                attributes.put(attribute, new AttributeDate(attribute));
            }
        
            attributes.get(attribute).setValue(followUpECPTxDates.get(i));
        }
    }
    
    public String moreTxDatesAction() {
        followUpECPTxDates.add(null);
        followUpECPTxDates.add(null);
        followUpECPTxDates.add(null);
        
        // Stay on same page
        return null;
    }
}
