package edu.wustl.mir.ctt.calc;

import edu.wustl.mir.ctt.form.StudyArmEligibilityForm;
import edu.wustl.mir.ctt.model.DeclineStrata;
import edu.wustl.mir.ctt.model.PulmonaryEvaluation;
import edu.wustl.mir.ctt.model.Site;
import edu.wustl.mir.ctt.model.SpirometryStrata;
import edu.wustl.mir.ctt.util.DateUtil;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.time.DateUtils;
import edu.wustl.mir.ctt.log.AuditLogger;

/**
 * Screen the participant for eligibility and study-arm assignment.
 * 
 * Uses all measurements provided. Only screens for minimum data requirements.
 *
 * @author drm
 */
public class StudyArmEligibilityCalculator implements Serializable {
    
    private enum EligibilityStatus {
        INELIGIBLE_TOO_FEW_FEV1S_BEFORE_ENROLLMENT, // fewer than five sufficiently spaced FEV1s before enrollment
        ELIGIBLE_STRATA_HIGH_DECLINE,               // slope < -30 and pvalue < 0.05
        ELIGIBLE_STRATA_LOW_DECLINE,                // slope < -30 and pvalue < 0.05
        INELIGIBLE_INSIGNIFICANT,                   // slope < -30 and pvalue >= 0.05
        INELIGIBLE_FEV1_DECLINE_NOT_ENOUGH,         // slope >= -30 
        INELIGIBLE_WBC_COUNT_TOO_LOW,               // wbc count below threshold for leukopenia (3000 cells/mm3)
        INELIGIBLE_LATEST_EXAM_TOO_OLD,             // latest physical exam is older than two weeks
        INELIGIBLE_LATEST_CBC_TOO_OLD,              // latest complete blood count is older than two weeks
        INELIGIBLE_MEASUREMENTS_NOT_REGULAR,        // fev1s in the past 4 months have interval greater than 8 weeks
        INELIGIBLE_MEASUREMENTS_NOT_REGULAR_LONG,   // fev1s in the past 4 months have interval greater than 8 weeks
        INELIGIBLE_TOO_FEW_FEV1S_BEFORE_DIAGNOSIS,  // fewer than three sufficiently spaced FEV1s before diagnosis
        INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET,      // diagnosis FEV1s are not 20% lower than ISHLT baseline
        INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET_2,    // diagnosis FEV1s are not at least three weeks apart
        INELIGIBLE_DIAGNOSIS_PFT_INCORRECT,         // first diagnosis FEV1 should be used as diagnosis date
        INELIGIBLE_DIAGNOSIS_TOO_OLD_NINE,          // diagnosis date must be within 9 weeks in protocol 8.0
        INELIGIBLE_DIAGNOSIS_TOO_OLD_SIX            // diagnosis date must be within 6 weeks in protocol 7.1
    }
    
    // Use status enumeration as key for message map for readability
    private static final Map<EligibilityStatus, String> rules;
    static { // Initialize static map, see https://stackoverflow.com/a/507658
        Map<EligibilityStatus, String> r = new EnumMap<>(EligibilityStatus.class);
        r.put(EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_BEFORE_ENROLLMENT, "In the months before enrollment specified in the protocol, there are not at least 5 measurements that are all spaced at least 14 days apart.");
        r.put(EligibilityStatus.ELIGIBLE_STRATA_HIGH_DECLINE, "meets the criteria (slope < -30 AND p-value < 0.05).");
        r.put(EligibilityStatus.ELIGIBLE_STRATA_LOW_DECLINE, "meets the criteria (slope < -30 AND p-value < 0.05).");
        r.put(EligibilityStatus.INELIGIBLE_INSIGNIFICANT, "The p-value >= 0.05 and the slope < -30.");
        r.put(EligibilityStatus.INELIGIBLE_FEV1_DECLINE_NOT_ENOUGH, "The slope >= -30.");
        r.put(EligibilityStatus.INELIGIBLE_WBC_COUNT_TOO_LOW, "The white blood cell count is less than 3 K/cumm.");
        r.put(EligibilityStatus.INELIGIBLE_LATEST_EXAM_TOO_OLD, "The documented physical assessment must be within two weeks prior to enrollment.");
        r.put(EligibilityStatus.INELIGIBLE_LATEST_CBC_TOO_OLD, "The documented complete blood count must be within two weeks prior to enrollment.");
	r.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET, "The laboratory based FEV1 values used to diagnose BOS were not at least 20% lower than baseline FEV1.");
        r.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET_2, "The laboratory based FEV1 values used to diagnose BOS were not at least three weeks apart.");
        r.put(EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR, "The measurements obtained in the last four months were not regular (interval between measurements exceeded eight weeks).");
        r.put(EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR_LONG, "The measurements obtained in the last six months were not regular (interval between measurements exceeded twelve weeks).");
        r.put(EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_BEFORE_DIAGNOSIS, "In the months before diagnosis specified in the protocol, there are not at least 3 measurements that are all spaced at least 14 days apart.");
        r.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_TOO_OLD_NINE, "The diagnosis date must be within nine weeks prior to enrollment.");
        r.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_TOO_OLD_SIX, "The diagnosis date must be within six weeks prior to enrollment.");
        r.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_PFT_INCORRECT, "DATA ENTRY ERROR NOTED: Laboratory-based FEV1 values used to confirm the initial diagnosis of NEW BOS is defined as the date at which the first FEV1 value recorded fell below the threshold. Please click the Review Data Entry button below and confirm data entered in Section D.");
        
        rules = Collections.unmodifiableMap(r);
    }
   
    private static final Map<EligibilityStatus, String> msgs;
    static { // Initialize static map, see https://stackoverflow.com/a/507658
        Map<EligibilityStatus, String> m = new EnumMap<>(EligibilityStatus.class);
        m.put(EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_BEFORE_ENROLLMENT, "Patient not eligible");
        m.put(EligibilityStatus.ELIGIBLE_STRATA_HIGH_DECLINE, "Patient is eligible. Complete enrollment eligibility forms on line to confirm eligibility");
        m.put(EligibilityStatus.ELIGIBLE_STRATA_HIGH_DECLINE, "Patient is eligible. Complete enrollment eligibility forms on line to confirm eligibility");
        m.put(EligibilityStatus.INELIGIBLE_INSIGNIFICANT, "Patient not eligible");
        m.put(EligibilityStatus.INELIGIBLE_FEV1_DECLINE_NOT_ENOUGH, "Patient not eligible");
        m.put(EligibilityStatus.INELIGIBLE_WBC_COUNT_TOO_LOW, "Patient not eligible");
        m.put(EligibilityStatus.INELIGIBLE_LATEST_EXAM_TOO_OLD, "Patient not eligible");
        m.put(EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR, "Patient not eligible");
        m.put(EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR_LONG, "Patient not eligible");
        m.put(EligibilityStatus.INELIGIBLE_LATEST_CBC_TOO_OLD, "Patient not eligible");
        m.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET, "Patient not eligible");
        m.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET_2, "Patient not eligible");
        m.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_TOO_OLD_NINE, "Patient not eligible");
        m.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_TOO_OLD_SIX, "Patient not eligible");
        m.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_PFT_INCORRECT, "Data entry issue.");
        msgs = Collections.unmodifiableMap(m);
    }

    private SlopeEstimator estimator;
    private EligibilityStatus status;
    private float minFev;
    private float lastFev;
    private float slope;
    private float pvalue;
    private List<PulmonaryEvaluation> evals;
    private List<PulmonaryEvaluation> evalsWithQualifyingDates;
    private Date screenDate;
    private String fev1OldestPossibleDate;
    private Date lastExamDate;
    private Date lastCbcDate;
    private Float wbcs;
    private Float baseline;
    private List<Float> diagFEVs;
    private Date diagDate;
    private List<Float> diagFEVValues;
    private Date earliestDiagFEVDate;
    private Date latestDiagFEVDate;
    private DeclineStrata declineStrata;
    private SpirometryStrata spirometryStrata = SpirometryStrata.LAB_SPIROMETRY_STRATA; // All subjects are lab spirometry until protocol revision
    
    private AuditLogger logger;
    
    public StudyArmEligibilityCalculator( StudyArmEligibilityForm form, Site site) {
        this.logger = AuditLogger.create(StudyArmEligibilityCalculator.class);
        
        this.evals = form.getPulmEvaluations();
        this.screenDate = form.getDate();
        this.lastExamDate = form.getMostRecentExamDate();
        this.lastCbcDate = form.getCompleteBloodCountDate();
        this.wbcs = form.getWbcs();
        
        this.baseline = form.getBaselineFEV1();
        
        this.diagFEVs = new ArrayList<>();
        this.diagFEVs.add(form.getFirstPostTransBOSDiagFEV1());
        this.diagFEVs.add(form.getSecondPostTransBOSDiagFEV1());
        if(form.getThirdPostTransBOSDiagFEV1() != null) {
            this.diagFEVs.add(form.getThirdPostTransBOSDiagFEV1());
        }
        
        this.diagDate = form.getPostTransBOSDiagDate();
        logger.audit("Setting nBOS diagnosis date to " + form.getPostTransBOSDiagDate());
        
        this.earliestDiagFEVDate = form.getFirstPostTransBOSDiagFEV1Date();
        logger.audit("Setting earliest diagnosis FEV1 date to " + form.getFirstPostTransBOSDiagFEV1Date());
        if (form.getSecondPostTransBOSDiagFEV1Date().before(earliestDiagFEVDate)) {
            logger.audit("Setting earliest diagnosis FEV1 date to " + form.getSecondPostTransBOSDiagFEV1Date());
            this.earliestDiagFEVDate = form.getSecondPostTransBOSDiagFEV1Date();
        }
        if (form.getThirdPostTransBOSDiagFEV1Date() != null && form.getThirdPostTransBOSDiagFEV1Date().before(earliestDiagFEVDate)) {
            logger.audit("Setting earliest diagnosis FEV1 date to " + form.getThirdPostTransBOSDiagFEV1Date());
            this.earliestDiagFEVDate = form.getThirdPostTransBOSDiagFEV1Date();
        }
        
        this.latestDiagFEVDate = form.getFirstPostTransBOSDiagFEV1Date();
        if (form.getSecondPostTransBOSDiagFEV1Date().after(latestDiagFEVDate)) {
            logger.audit("Setting latest diagnosis FEV1 date to " + form.getSecondPostTransBOSDiagFEV1Date());
            this.latestDiagFEVDate = form.getSecondPostTransBOSDiagFEV1Date();
        }
        if (form.getThirdPostTransBOSDiagFEV1Date() != null && form.getThirdPostTransBOSDiagFEV1Date().after(latestDiagFEVDate)) {
            logger.audit("Setting latest diagnosis FEV1 date to " + form.getThirdPostTransBOSDiagFEV1Date());
            this.latestDiagFEVDate = form.getThirdPostTransBOSDiagFEV1Date();
        }
        
        status = screen(true, false, site);
    }
    
	public StudyArmEligibilityCalculator( List<PulmonaryEvaluation> evals, Date screenDate, Date diagnosisDate, boolean crossover, Site site) {
        this.logger = AuditLogger.create(StudyArmEligibilityCalculator.class);
        
        this.evals = evals;
        this.screenDate = screenDate;
        this.diagDate = diagnosisDate;
       
        this.lastExamDate = null;
        this.lastCbcDate = null;
        this.wbcs = null;
        this.baseline = null;
        this.diagFEVs = null;
        
        this.earliestDiagFEVDate = null;
        
        logger.audit("In the StudyArmEligibilityCalculator, crossover was " + crossover + " in constructor");
        
//        System.out.println("The screenDate is the form.getDate: " + screenDate);
        // do not do inclusion/exclusion criteria checks
        status = screen(false, crossover, site);
    }

    public List<PulmonaryEvaluation> getEvaluations() {
        return evals;
    }

    public List<PulmonaryEvaluation> getEvalsWithQualifyingDates() {
        return evalsWithQualifyingDates;
    }

    public Date getScreenDate() {
        return screenDate;
    }

    public void setScreenDate(Date screenDate) {
        this.screenDate = screenDate;
    }
    
    public String getOutcomeMessage() {
        return msgs.get(status);
    }
    
    public String getOutcomeRule() {
        return rules.get(status);
    }
    
    public float getPValue() {
        return pvalue;
    }
    
    public float getSlope() {
        return slope;
    }
    
    public double predict( Date d) {
        // If you get an error message whne trying to run the estimator.predict(d) command, it most likely is due to the esitmator not being
        // instantiated in the initialScreen() method below (see the line of code 'estimator = new SlopeEstimator( dates, fevs)' in the initialScreen() method).
        return estimator.predict(d);
    }
    
    public float getMinFev() {
        return minFev;
    }
    
    public float getLastFev() {
        return lastFev;
    }
    
    public String getFev1OldestPossibleDate() {
        return fev1OldestPossibleDate;
    }
    
    public String getStudyEligibilityOutcome() {
        String outcome = "";
        
        if (isStudyEligible()) {
            outcome = "Participant is ELIGIBLE to be enrolled into the study.";
        } else {
            outcome = "Participant is NOT ELIGIBLE to be enrolled into the study. ";
        }
        
        return outcome;
    }
    
    public boolean isStudyEligible() {
        return (status == EligibilityStatus.ELIGIBLE_STRATA_HIGH_DECLINE ) || (status == EligibilityStatus.ELIGIBLE_STRATA_LOW_DECLINE);
    }
    
    public SpirometryStrata getSpirometryStrata() {
        return spirometryStrata;
    }
    
    public DeclineStrata getDeclineStrata() {
        return declineStrata;
        }
        
    public boolean isDeclineScreeningPassed() {
        return (status == EligibilityStatus.ELIGIBLE_STRATA_HIGH_DECLINE ||
                status == EligibilityStatus.ELIGIBLE_STRATA_LOW_DECLINE);
    }
    
    public boolean isSlopeOKandStatsSig() {
        return (status == EligibilityStatus.ELIGIBLE_STRATA_HIGH_DECLINE ||
                status == EligibilityStatus.ELIGIBLE_STRATA_LOW_DECLINE);
    }
    
    public boolean isSlopeOKandStatsNotSig() {
        return (status == EligibilityStatus.INELIGIBLE_INSIGNIFICANT);
    }
    
    public boolean isDataStale() {
        return status == EligibilityStatus.INELIGIBLE_DIAGNOSIS_TOO_OLD_SIX || status == EligibilityStatus.INELIGIBLE_DIAGNOSIS_TOO_OLD_NINE;
    }
    
    public boolean isDataTooFew() {
        return status == EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_BEFORE_ENROLLMENT;
    }
    
    public int numberOfMinimallySpacedMeasurementsInPeriod( List<PulmonaryEvaluation> evals, Date startDate, Date stopDate, float spacingInDays) {
        evalsWithQualifyingDates = filterForMinimalSpacing( evals, spacingInDays);
        
//        int j;
//        for(int i=0; i < qualifyingDates.size(); i++){
//            j = i + 1;
//            System.out.println("The qualifying dates are: " + j + "   " + qualifyingDates.get(i).getDate().toString());
//        }
        return countInInterval( evalsWithQualifyingDates, startDate, stopDate);
    }
    
    public List<PulmonaryEvaluation> filterForMinimalSpacing( List<PulmonaryEvaluation> evals, float spacingInDays) {
        evalsWithQualifyingDates = new ArrayList<>();
        
        if( evals.size() > 1) {
            evalsWithQualifyingDates.add( evals.get(0));
            Date d1, d2;
            for( int i = 0; i < evals.size(); i++) {
                d1 = evals.get(i).getDate();
                for( int j = i+1; j < evals.size(); j++) {
                    d2 = evals.get(j).getDate();
                    // The minus 0.1 needed to be taken away from the spacingInDays because of the change
                    // in Central Standard Time (CST) to Daylight Savings Time (DST).  The day of the time  
                    // change, the number of hours in a day are 23 not 24 so a day is less than 1, but we 
                    // expect the days in the interval to all have 24 hours.  The minus 0.1 takes care of 
                    // the change in time in the spring.
                    if( DateUtil.intervalInDays( d1, d2) >= spacingInDays - 0.1) {
                        evalsWithQualifyingDates.add( evals.get(j));
                        logger.audit("Adding qualifying date " + evals.get(j).getDate());
                        i = j - 1;
                        break;
                    } else {
                        logger.audit("Rejecting date " + evals.get(j).getDate() + " because interval is " + DateUtil.intervalInDays( d1, d2));
                    }
                }
            }
        }
        
        return evalsWithQualifyingDates;
    }
    
    public int countInInterval( List<PulmonaryEvaluation> evals, Date startDate, Date stopDate) {
        int cnt = 0;
        double days;
        days = DateUtil.intervalInDays(startDate, stopDate);
        for( PulmonaryEvaluation e: evals) {
            if( DateUtil.isDateInIntervalInclusive(e.getDate(), startDate, stopDate)) {
                cnt++;
            }
        }
        System.out.println("The count in the 6 month interval is: " + cnt);
        return cnt;
    }
    
    public boolean wereMeasurementsObtainedRegularly(List<PulmonaryEvaluation> evals, int thresholdMonths, int intervalDays) {
        boolean measurementsSufficientlySpaced = true;
        List<PulmonaryEvaluation> filteredEvals = new ArrayList<>();
        
        // Sort the array of PulmonaryEvaluation objects by date in ascending order.
        Collections.sort(evals, new Comparator<PulmonaryEvaluation>() {
            @Override
            public int compare(PulmonaryEvaluation pe1, PulmonaryEvaluation pe2) {
                if (pe1.getDate() == null || pe2.getDate() == null)
                    return 0;
                return pe1.getDate().compareTo(pe2.getDate());
            }
        });
        
        // Only add dates at the threshold or newer to the list
        Date monthThreshold = DateUtil.addMonths(screenDate, -thresholdMonths);
        for (PulmonaryEvaluation p : evals) {
            if(p.getDate().equals(monthThreshold) || p.getDate().after(monthThreshold)){
                logger.audit("Adding date " + p.getDate());
                filteredEvals.add(p);
            } else {
                logger.audit("Rejecting date " + p.getDate());
                logger.audit(p.getDate() + " is before threshold " + monthThreshold + "? " + p.getDate().before(monthThreshold));
            }
        }
        
        for (int i = 0; (i + 1) < filteredEvals.size(); i++) {
            Date currentDate = filteredEvals.get(i).getDate();
            Date intervalThreshold = DateUtil.addDays(currentDate, intervalDays);
            Date nextDate = filteredEvals.get(i + 1).getDate();
            
            // is the next measurement further away than the threshold? (greater than required spacing?)
            if (nextDate.after(intervalThreshold)) {
                measurementsSufficientlySpaced = false;
            }
        }
        
        return measurementsSufficientlySpaced;
    }
    
    private boolean isMeasurementRecentEnough( Date d, Date now) {
        double days = DateUtil.intervalInDays(d, now);
        return days < 22;
//        Date startDate = DateUtil.addDays(now, -7);
//        return DateUtil.isDateInIntervalInclusive( d, startDate, now);
    }
    
    private boolean areDiagnosisEvaluationsSpaced( Date firstDiag, Date secondDiag) {
        Date threshold = DateUtil.addDays(firstDiag, 21);
        
        return (secondDiag.compareTo(threshold) >= 0); // -1 compare result means that the second diagnosis PFT took place before 21 days, too soon 
    }
    
    private Date getDateOfLastEvaluation() {
        Date d = null;
        if( evals.size() > 0) {
            d = evals.get(0).getDate();
            for( PulmonaryEvaluation e: evals) {
                if( e.getDate().after(d)) {
                    d = e.getDate();
                }
            }
        }
        return d;
    }
    
    public List<PulmonaryEvaluation> getEvalsWithinLastMonths(List<PulmonaryEvaluation> pulmEvals, Date screenDate, int numMonths, int numDays) {
//        List<PulmonaryEvaluation> pulmEvals = new ArrayList<PulmonaryEvaluation>();
        List<PulmonaryEvaluation> pulmEvals2 = new ArrayList<>();
        
        // Sort the array of PulmonaryEvaluation objects by date in ascending order.
        Collections.sort(pulmEvals, new Comparator<PulmonaryEvaluation>() {
            @Override
            public int compare(PulmonaryEvaluation pe1, PulmonaryEvaluation pe2) {
                if (pe1.getDate() == null || pe2.getDate() == null)
                    return 0;
                return pe1.getDate().compareTo(pe2.getDate());
            }
        });

        // Remove any dates older than six months from the list.
        PulmonaryEvaluation pe;
        Date threshold = DateUtil.addMonths(screenDate, -numMonths);
        threshold = DateUtil.addDays(threshold, -numDays);
        logger.audit("getEvalsWithinLastMonths threshold is " + threshold);
        
        Iterator iterator = pulmEvals.iterator();
        while(iterator.hasNext()){
            pe = (PulmonaryEvaluation) iterator.next();
            
            if (!pe.getDate().before(threshold)) {
                pulmEvals2.add(pe);
            }
        }

        return pulmEvals2;

    }
    
    private EligibilityStatus screen(boolean enrollmentScreen, boolean crossoverScreen, Site site) {
        String crfString = site.getCrfVersion();
        EligibilityStatus status;
        
        if (crfString.equals("7.1")) {
            status = screen_7_1(enrollmentScreen, crossoverScreen);
        } else {
            status = screen_8_0(enrollmentScreen, crossoverScreen);
        }
        
        return status;
    }
    
    private EligibilityStatus screen_7_1(boolean enrollmentScreen, boolean crossoverScreen) {
        List<Date> dates = new ArrayList<>();
        List<Float> fevs = new ArrayList<>();
        EligibilityStatus i;
        
        logger.audit("In StudyArmEligibilityCalculator.screen, enrollmentscreen is " + enrollmentScreen + " and crossoverscreen is " + crossoverScreen);
        
        if (enrollmentScreen) {
            logger.audit("Checking enrollment screening...");
            if (wbcs != null && wbcs < 3.0) {
                i = EligibilityStatus.INELIGIBLE_WBC_COUNT_TOO_LOW;
                return i;
            }

            if (lastExamDate != null && DateUtil.intervalInDays(lastExamDate, screenDate) > 15) {
                i = EligibilityStatus.INELIGIBLE_LATEST_EXAM_TOO_OLD;
                return i;
            }

            if (lastCbcDate != null && DateUtil.intervalInDays(lastCbcDate, screenDate) > 15) {
                i = EligibilityStatus.INELIGIBLE_LATEST_CBC_TOO_OLD;
                return i;
            }

            // Check that BOS diagnosis values are 20% less than baseline
            if (baseline != null && diagFEVs != null) {
                Float threshold = baseline * 0.805f;

                for (Float diagFEV : diagFEVs) {
                    if (diagFEV > threshold) {
                        i = EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET;
                        return i;
                    }
                }
            }
            
            // Check that diagnosis PFTs are appropriately spaced
            if (earliestDiagFEVDate != null && latestDiagFEVDate != null) {
                if (!areDiagnosisEvaluationsSpaced(earliestDiagFEVDate, latestDiagFEVDate)) {
                    i = EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET_2;
                    return i;
                }
            }
        }
        
        int diagAllowedWeeks = 6;
        // Add an extra day to the threshold because day starts at midnight, needs to be midnight at the too old day
        Date diagThreshold = DateUtils.addDays(screenDate, -((diagAllowedWeeks * 7) + 1));
        logger.audit("Diagnosis date threshold set at " + diagThreshold);
        if(diagDate.before(diagThreshold)) {
            i = EligibilityStatus.INELIGIBLE_DIAGNOSIS_TOO_OLD_SIX;
            return i;
        }
        
        // Format the start of the six month period so the oldest possible FEV1 date can be determined.
        // The fev1OldestPossibleDate is used enrollStudyArmElig.xhtml form in the Omnifaces o:validateMultiple
        // validators to let the individual filling out the form know the oldest possible FEV1 date that can be
        // entered into the form.
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date fev1SixMonthsPrevious = DateUtil.addMonths(screenDate, -6);
        Date fev1SixMonthsThreshold = DateUtil.addDays(fev1SixMonthsPrevious, -1);
        
        String fev1OldestDate = sdf.format(fev1SixMonthsThreshold);
        
        // If a oldest date is six months ago, use the date below to show the person entering the dates on the Elig Form.
        // We do not want to show them a date of exactly six months as above.
        fev1OldestPossibleDate = sdf.format(fev1SixMonthsPrevious);

        // Use exactly six months for calculation
        Date startDate = DateUtil.addMonths(screenDate, -6);
        System.out.println("The startDate is the screenDate - 6 months: " + startDate);
        System.out.println("The stopDate is the screenDate: " + screenDate);
        
        // Get the FEV1 Dates and values within the last six months from the initialScreen date.
        evalsWithQualifyingDates = this.getEvalsWithinLastMonths(evals, screenDate, 6, 0);
        
        // SPECIAL NOTE: The number fourteen (14) used in the call to numberOfMinimallySpacedMeasurementsInPeriod refers to the
        // number of days in the spacing between measurement periods.
        // NOTE: The change from Central Standard Time to Central Daylight Time in the spring causes one day to be only 23 hours long, so
        // a full period does not occur between two dates if the spring time date change is between the dates.
        // If you follow the method numberOfMinimallySpacedMeasurementsInPeriod you will find we subtracted 0.1 days from the period to account
        // for the change in time.
        int countMeasurementsSufficientlySpaced = numberOfMinimallySpacedMeasurementsInPeriod( evalsWithQualifyingDates, startDate, screenDate, 14);
        System.out.println("The countMeasurementsSufficientlySpaced is: " + countMeasurementsSufficientlySpaced + "\n");
        if( countMeasurementsSufficientlySpaced < 5) {
            status = EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_BEFORE_ENROLLMENT;
            return status;
        }
        
        // Check to make sure that there are measurements 
        if (! wereMeasurementsObtainedRegularly(evalsWithQualifyingDates, 6, 57)) {
            status = EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR;
            return status;
        }
            
       /* if (!crossoverScreen) {
            if (diagDate != null) {
                int countMeasurementsSpacedBeforeDiagnosis = numberOfMinimallySpacedMeasurementsInPeriod( evalsWithQualifyingDates, startDate, diagDate, 14);
                logger.audit("The countMeasurementsSpacedBeforeDiagnosis is: " + countMeasurementsSpacedBeforeDiagnosis + "\n");
                
                if( countMeasurementsSpacedBeforeDiagnosis < 3) {
                    status = EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_BEFORE_DIAGNOSIS;
                    return status;
                }
                
            } else {
                status = EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_BEFORE_DIAGNOSIS;
                return status;
            }
        }*/

        for( PulmonaryEvaluation eval: evalsWithQualifyingDates) {
            dates.add(eval.getDate());
            fevs.add(eval.getFev1() * 1000f);
        }
                
        int nValues = fevs.size();
        lastFev = fevs.get(nValues - 1);
        //int indexOfMinValue = fevs.indexOf( Collections.min(fevs));
        //minFev = fevs.get(indexOfMinValue);
        minFev = Collections.min(fevs);
        
        estimator = new SlopeEstimator( dates, fevs);
        
        slope = (float) estimator.getSlope();
        pvalue = (float) estimator.getPValue();
        
        if( pvalue < 0.05) {
            if( slope < -30) {
                if (slope >= -200) {
                    i = EligibilityStatus.ELIGIBLE_STRATA_LOW_DECLINE; // -200 >= slope < -30 and pvalue < 0.05
                    declineStrata = DeclineStrata.LOW_DECLINE_STRATA;
                } else {
                    i = EligibilityStatus.ELIGIBLE_STRATA_HIGH_DECLINE;  // slope < -200 and pvalue < 0.05
                    declineStrata = DeclineStrata.HIGH_DECLINE_STRATA;
                }
            }
            else { // slope >= -30
                i = EligibilityStatus.INELIGIBLE_FEV1_DECLINE_NOT_ENOUGH;    // minFev >= 1200 and slope >= -30
            }
        } else { // pvalue >= 0.05
            i = EligibilityStatus.INELIGIBLE_INSIGNIFICANT;    // minFev >= 1200 and slope < -30 and pvalue >= 0.05
        }
            
        return i;
    }
    
        private EligibilityStatus screen_8_0(boolean enrollmentScreen, boolean crossoverScreen) {
        List<Date> dates = new ArrayList<>();
        List<Float> fevs = new ArrayList<>();
        EligibilityStatus i;
        
        logger.audit("In StudyArmEligibilityCalculator.screen, enrollmentscreen is " + enrollmentScreen + " and crossoverscreen is " + crossoverScreen);
        
        if (enrollmentScreen) {
            logger.audit("Checking enrollment screening...");
            if (wbcs != null && wbcs < 3.0) {
                i = EligibilityStatus.INELIGIBLE_WBC_COUNT_TOO_LOW;
                return i;
            }

            if (lastExamDate != null && DateUtil.intervalInDays(lastExamDate, screenDate) > 15) {
                i = EligibilityStatus.INELIGIBLE_LATEST_EXAM_TOO_OLD;
                return i;
            }

            if (lastCbcDate != null && DateUtil.intervalInDays(lastCbcDate, screenDate) > 15) {
                i = EligibilityStatus.INELIGIBLE_LATEST_CBC_TOO_OLD;
                return i;
            }

            // Check that BOS diagnosis values are 20% less than baseline
            if (baseline != null && diagFEVs != null) {
                Float threshold = baseline * 0.805f;

                for (Float diagFEV : diagFEVs) {
                    if (diagFEV > threshold) {
                        i = EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET;
                        return i;
                    }
                }
            }
        
            // Check that diagnosis date is set at date of earliest diagnosis FEV1
            if (earliestDiagFEVDate != null) {
                if (!earliestDiagFEVDate.equals(diagDate)) {
                    i = EligibilityStatus.INELIGIBLE_DIAGNOSIS_PFT_INCORRECT;
                    return i;
                }
            }
            
            // Check that diagnosis PFTs are appropriately spaced
            if (earliestDiagFEVDate != null && latestDiagFEVDate != null) {
                if (!areDiagnosisEvaluationsSpaced(earliestDiagFEVDate, latestDiagFEVDate)) {
                    i = EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET_2;
                    return i;
                }
            }
        }
        
        int diagAllowedWeeks = 9;
        // Add an extra day to the threshold because day starts at midnight, needs to be midnight at the too old day
        Date diagThreshold = DateUtils.addDays(screenDate, -((diagAllowedWeeks * 7) + 1));
        logger.audit("Diagnosis date threshold set at " + diagThreshold);
        if(diagDate.before(diagThreshold)) {
            i = EligibilityStatus.INELIGIBLE_DIAGNOSIS_TOO_OLD_NINE;
            return i;
        }
        
        // Format the start of the nine month period so the oldest possible FEV1 date can be determined.
        // The fev1OldestPossibleDate is used enrollStudyArmElig.xhtml form in the Omnifaces o:validateMultiple
        // validators to let the individual filling out the form know the oldest possible FEV1 date that can be
        // entered into the form.
        int monthsPrevious = 6;
        int additionalDaysPrevious = 0; // used for waiver
        int monthsSpaced = 6;
        int spacing = 57; // eight weeks
        List<PulmonaryEvaluation> evalsBeforeDiagnosis;
        boolean passedRegularMeasurements = true;
        boolean passedEnoughEvals = true;
        
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date fev1MonthsBeforeEnrollment = DateUtil.addMonths(screenDate, -6);
        Date fev1MonthsCalculationThreshold = DateUtil.addDays(fev1MonthsBeforeEnrollment, -1);
        
        String fev1OldestDate = sdf.format(fev1MonthsCalculationThreshold);
        
        // If a oldest date is six months ago, use the date below to show the person entering the dates on the Elig Form.
        // We do not want to show them a date of exactly six months as above.
        fev1OldestPossibleDate = sdf.format(fev1MonthsBeforeEnrollment);

        // Use exactly six months for calculation
        Date startDate = DateUtil.addMonths(screenDate, -monthsPrevious);
        logger.audit("The calculation startDate is the screenDate - " + monthsPrevious + " months: " + startDate);
        logger.audit("The calculation stopDate is the screenDate: " + screenDate);
        
        // Get the FEV1 Dates and values within the last six months from the initialScreen date.
        evalsWithQualifyingDates = this.getEvalsWithinLastMonths(evals, screenDate, monthsPrevious, additionalDaysPrevious);
        
        int countMeasurementsSufficientlySpaced = numberOfMinimallySpacedMeasurementsInPeriod( evalsWithQualifyingDates, startDate, screenDate, 14);
        logger.audit("The countMeasurementsSufficientlySpaced is: " + countMeasurementsSufficientlySpaced + "\n");
        
        int minimumNumEvals = 5; // may change for protocol exception
        
        if( countMeasurementsSufficientlySpaced < minimumNumEvals) {
            passedEnoughEvals = false;
        } else {
            passedEnoughEvals = true;
        }
        
        if (passedEnoughEvals == true) {
            evalsBeforeDiagnosis = this.getEvalsWithinLastMonths(evals, diagDate, monthsSpaced, 0);
            
            if (wereMeasurementsObtainedRegularly(evalsBeforeDiagnosis, monthsSpaced, spacing)) {
                passedRegularMeasurements = true;
            } else {
                passedRegularMeasurements = false;
            }
        }
        
        // fall back to nine months if needed
        if (passedEnoughEvals == false || passedRegularMeasurements == false) {
            monthsPrevious = 9;
            monthsSpaced = 9;
            spacing = 85; // twelve weeks
            
            fev1MonthsBeforeEnrollment = DateUtil.addMonths(screenDate, -monthsPrevious);
            fev1MonthsCalculationThreshold = DateUtil.addDays(fev1MonthsBeforeEnrollment, -1);
            
            fev1OldestDate = sdf.format(fev1MonthsCalculationThreshold);
            fev1OldestPossibleDate = sdf.format(fev1MonthsBeforeEnrollment);
            
            // Use exactly nine months for calculation
            startDate = DateUtil.addMonths(screenDate, -monthsPrevious);
            logger.audit("The calculation startDate is the screenDate - " + monthsPrevious + " months: " + startDate);
            logger.audit("The calculation stopDate is the screenDate: " + screenDate);

            // Get the FEV1 Dates and values within the last six months from the initialScreen date.
            evalsWithQualifyingDates = this.getEvalsWithinLastMonths(evals, screenDate, monthsPrevious, additionalDaysPrevious);

            countMeasurementsSufficientlySpaced = numberOfMinimallySpacedMeasurementsInPeriod( evalsWithQualifyingDates, startDate, screenDate, 14);
            logger.audit("The countMeasurementsSufficientlySpaced is: " + countMeasurementsSufficientlySpaced + "\n");

            minimumNumEvals = 5; // may change for protocol exception

            if( countMeasurementsSufficientlySpaced < minimumNumEvals) {
                passedEnoughEvals = false;
            } else {
                passedEnoughEvals = true;
            }

            if (passedEnoughEvals == true) {
                evalsBeforeDiagnosis = this.getEvalsWithinLastMonths(evals, diagDate, monthsSpaced, 0);

                if (wereMeasurementsObtainedRegularly(evalsBeforeDiagnosis, monthsSpaced, spacing)) {
                    passedRegularMeasurements = true;
                } else {
                    passedRegularMeasurements = false;
                }
            }
        }
        
        if (false == passedEnoughEvals) {
            i = EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_BEFORE_ENROLLMENT;
            return i;
        } else if (false == passedRegularMeasurements) {
            i = EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR_LONG;
            return i;
        }
            
        if (!crossoverScreen) {
            if (diagDate != null) {
                int countMeasurementsSpacedBeforeDiagnosis = numberOfMinimallySpacedMeasurementsInPeriod( evalsWithQualifyingDates, startDate, diagDate, 14);
                logger.audit("The countMeasurementsSpacedBeforeDiagnosis is: " + countMeasurementsSpacedBeforeDiagnosis + "\n");
                if( countMeasurementsSpacedBeforeDiagnosis < 3) {
                    status = EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_BEFORE_DIAGNOSIS;
                    return status;
                }
            } else {
                status = EligibilityStatus.INELIGIBLE_DIAGNOSIS_PFT_INCORRECT;
                return status;
            }
        }

        for( PulmonaryEvaluation eval: evalsWithQualifyingDates) {
            dates.add(eval.getDate());
            fevs.add(eval.getFev1() * 1000f);
        }
                
        int nValues = fevs.size();
        lastFev = fevs.get(nValues - 1);
        //int indexOfMinValue = fevs.indexOf( Collections.min(fevs));
        //minFev = fevs.get(indexOfMinValue);
        minFev = Collections.min(fevs);
        
        estimator = new SlopeEstimator( dates, fevs);
        
        slope = (float) estimator.getSlope();
        pvalue = (float) estimator.getPValue();
        
        if( pvalue < 0.05) {
            if( slope < -30) {
                if (slope >= -200) {
                    i = EligibilityStatus.ELIGIBLE_STRATA_LOW_DECLINE; // -200 >= slope < -30 and pvalue < 0.05
                    declineStrata = DeclineStrata.LOW_DECLINE_STRATA;
                } else {
                    i = EligibilityStatus.ELIGIBLE_STRATA_HIGH_DECLINE;  // slope < -200 and pvalue < 0.05
                    declineStrata = DeclineStrata.HIGH_DECLINE_STRATA;
                }
            }
            else { // slope >= -30
                i = EligibilityStatus.INELIGIBLE_FEV1_DECLINE_NOT_ENOUGH;    // minFev >= 1200 and slope >= -30
            }
        } else { // pvalue >= 0.05
            i = EligibilityStatus.INELIGIBLE_INSIGNIFICANT;    // minFev >= 1200 and slope < -30 and pvalue >= 0.05
        }
            
        return i;
    }
}