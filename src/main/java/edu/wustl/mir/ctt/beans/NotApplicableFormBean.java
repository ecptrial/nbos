package edu.wustl.mir.ctt.beans;

import edu.wustl.mir.ctt.AEController;
import edu.wustl.mir.ctt.Controller;
import edu.wustl.mir.ctt.form.AdverseEventWorksheetSAEForm;
import edu.wustl.mir.ctt.form.BasicForm;
import edu.wustl.mir.ctt.model.ECPEventTypes;
import edu.wustl.mir.ctt.model.ECPEvents;
import edu.wustl.mir.ctt.model.ECPFormTypes;
import edu.wustl.mir.ctt.model.Event;
import edu.wustl.mir.ctt.model.EventStatus;
import edu.wustl.mir.ctt.model.FormStatus;
import edu.wustl.mir.ctt.model.StatusChangeEvent;
import edu.wustl.mir.ctt.notification.NotificationException;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.PersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.apache.logging.log4j.LogManager;
import edu.wustl.mir.ctt.log.AuditLogger;

/**
 *
 * @author drm
 */
@ManagedBean
@RequestScoped
public class NotApplicableFormBean implements Serializable {
    private AuditLogger logger = AuditLogger.create(NotApplicableFormBean.class);
    private Event event;
    private String notApplicableReason;
    private Date dateOfOccurance;
    private String title;
    private BasicForm form;
    
    @ManagedProperty(value="#{controller}")
    private Controller controller;
    
    public NotApplicableFormBean() {
    }
    
    @PostConstruct
    public void init() {
        event = controller.getSelectedEvent();
        form = controller.getSelectedForm();
    }
    
    public String getNotApplicableReason() { return notApplicableReason; }
    public void setNotApplicableReason(String u) { notApplicableReason = u; }
    public BasicForm getForm() { return form; }
    public void setForm(BasicForm f) { form = f; }
    
    public String notApplicableFormAction() throws PersistenceException, NotificationException {
        logger.audit("In NotApplicableFormBean.notApplicableFormAction, before action, event status is " + controller.getSelectedEvent().getStatus());
        logger.audit("In NotApplicableFormBean.notApplicableFormAction, before action, form status is " + controller.getSelectedForm().getStatus());

        // do unlock
        PersistenceManager pm = ServiceRegistry.getPersistenceManager();
        
        pm.changeEventStatus(event, EventStatus.NOT_APPLICABLE);
        pm.changeFormStatus(form, FormStatus.NOT_APPLICABLE);
        
        StatusChangeEvent sce = new StatusChangeEvent(form.getId(), form.getStatus(), FormStatus.NOT_APPLICABLE, controller.getCurrentUserName(), new Date(), "NotApplicableFormBean.notApplicableFormAction", notApplicableReason);
        pm.insertStatusChangeEvent(sce);
        
        // refresh event cache on controller
        controller.getEvents();
        
        logger.audit("In NotApplicableFormBean.notApplicableFormAction, after action, event status is " + controller.getSelectedEvent().getStatus());
        logger.audit("In NotApplicableFormBean.notApplicableFormAction, after action, form status is " + controller.getSelectedForm().getStatus());
        
        return controller.viewEventAction(event.getId());
    }

    public void setController(Controller c) {
        this.controller = c;
    }
    
    public Controller getController() {
        return controller;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
