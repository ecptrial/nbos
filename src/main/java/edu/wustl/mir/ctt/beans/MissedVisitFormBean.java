package edu.wustl.mir.ctt.beans;

import edu.wustl.mir.ctt.AEController;
import edu.wustl.mir.ctt.Controller;
import edu.wustl.mir.ctt.form.AdverseEventWorksheetSAEForm;
import edu.wustl.mir.ctt.form.BasicForm;
import edu.wustl.mir.ctt.model.ECPEventTypes;
import edu.wustl.mir.ctt.model.ECPEvents;
import edu.wustl.mir.ctt.model.ECPFormTypes;
import edu.wustl.mir.ctt.model.Event;
import edu.wustl.mir.ctt.model.EventStatus;
import edu.wustl.mir.ctt.model.FormStatus;
import edu.wustl.mir.ctt.model.Participant;
import edu.wustl.mir.ctt.model.Site;
import edu.wustl.mir.ctt.model.StatusChangeEvent;
import edu.wustl.mir.ctt.notification.NotificationContent;
import edu.wustl.mir.ctt.notification.NotificationException;
import edu.wustl.mir.ctt.notification.NotificationManager;
import edu.wustl.mir.ctt.notification.NotificationType;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.PersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import edu.wustl.mir.ctt.log.AuditLogger;

/**
 *
 * @author drm
 */
@ManagedBean
@RequestScoped
public class MissedVisitFormBean implements Serializable {
    private AuditLogger logger = AuditLogger.create(MissedVisitFormBean.class);
    private Event event;
    private String missedVisitReason;
    private Date dateOfOccurance;
    private String title;
    private BasicForm form;
    private Site site;
    private Participant participant;
    
    @ManagedProperty(value="#{controller}")
    private Controller controller;
    
    public MissedVisitFormBean() {
    }
    
    @PostConstruct
    public void init() {
        event = controller.getSelectedEvent();
        form = controller.getSelectedForm();
        site = controller.getSelectedSite();
        participant = controller.getSelectedParticipant();
    }
    
    public String getMissedVisitReason() { return missedVisitReason; }
    public void setMissedVisitReason(String u) { missedVisitReason = u; }
    public BasicForm getForm() { return form; }
    public void setForm(BasicForm f) { form = f; }
    
    public String missedVisitFormAction() throws PersistenceException, NotificationException {
        logger.audit("In MissedVisitFormBean.missedVisitFormAction, before action, event status is " + controller.getSelectedEvent().getStatus());
        logger.audit("In MissedVisitFormBean.missedVisitFormAction, before action, form status is " + controller.getSelectedForm().getStatus());

        // do missed visit
        PersistenceManager pm = ServiceRegistry.getPersistenceManager();
        
        pm.changeEventStatus(event, EventStatus.MISSED_VISIT);
        pm.changeFormStatus(form, FormStatus.MISSED_VISIT);
        
        StatusChangeEvent sce = new StatusChangeEvent(form.getId(), form.getStatus(), FormStatus.MISSED_VISIT, controller.getCurrentUserName(), new Date(), "MissedVisitFormBean.missedVisitFormAction", missedVisitReason);
        pm.insertStatusChangeEvent(sce);
        
        // send notification that visit was missed
        NotificationManager nm = ServiceRegistry.getNotificationManager();
        NotificationContent content = new NotificationContent();
        content.setSiteName( site.getName());
        content.setParticipantId( participant.getParticipantID());
        content.setStudyArm(participant.getStudyArmStatus().getName());
        content.setUserName(controller.getCurrentUserName());
        content.setTitle( event.getName());
        nm.send( NotificationType.MISSED_VISIT_ENTERED, content);
        
        // refresh event cache on controller
        controller.getEvents();
        
        logger.audit("In MissedVisitFormBean.missedVisitFormAction, after action, event status is " + controller.getSelectedEvent().getStatus());
        logger.audit("In MissedVisitFormBean.missedVisitFormAction, after action, form status is " + controller.getSelectedForm().getStatus());
        
        return controller.viewEventAction(event.getId());
    }

    public void setController(Controller c) {
        this.controller = c;
    }
    
    public Controller getController() {
        return controller;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
