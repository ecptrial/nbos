package edu.wustl.mir.ctt.beans;

import edu.wustl.mir.ctt.AEController;
import edu.wustl.mir.ctt.Controller;
import edu.wustl.mir.ctt.form.AdverseEventWorksheetSAEForm;
import edu.wustl.mir.ctt.form.BasicForm;
import edu.wustl.mir.ctt.model.ECPEventTypes;
import edu.wustl.mir.ctt.model.ECPEvents;
import edu.wustl.mir.ctt.model.ECPFormTypes;
import edu.wustl.mir.ctt.model.Event;
import edu.wustl.mir.ctt.model.EventStatus;
import edu.wustl.mir.ctt.model.FormStatus;
import edu.wustl.mir.ctt.model.StatusChangeEvent;
import edu.wustl.mir.ctt.notification.NotificationException;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.PersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import edu.wustl.mir.ctt.log.AuditLogger;

/**
 *
 * @author drm
 */
@ManagedBean
@RequestScoped
public class NotRequiredFormBean implements Serializable {
    private AuditLogger logger = AuditLogger.create(NotRequiredFormBean.class);
    private Event event;
    private String notRequiredReason;
    private Date dateOfOccurance;
    private String title;
    private BasicForm form;
    
    @ManagedProperty(value="#{controller}")
    private Controller controller;
    
    public NotRequiredFormBean() {
    }
    
    @PostConstruct
    public void init() {
        event = controller.getSelectedEvent();
        form = controller.getSelectedForm();
    }
    
    public String getNotRequiredReason() { return notRequiredReason; }
    public void setNotRequiredReason(String u) { notRequiredReason = u; }
    public BasicForm getForm() { return form; }
    public void setForm(BasicForm f) { form = f; }
    
    public String notRequiredFormAction() throws PersistenceException, NotificationException {
        logger.audit("In NotRequiredFormBean.notRequiredFormAction, before action, event status is " + controller.getSelectedEvent().getStatus());
        logger.audit("In NotRequiredFormBean.notRequiredFormAction, before action, form status is " + controller.getSelectedForm().getStatus());

        // do unlock
        PersistenceManager pm = ServiceRegistry.getPersistenceManager();
        
        pm.changeEventStatus(event, EventStatus.NOT_REQUIRED);
        pm.changeFormStatus(form, FormStatus.NOT_REQUIRED);
        
        StatusChangeEvent sce = new StatusChangeEvent(form.getId(), form.getStatus(), FormStatus.NOT_REQUIRED, controller.getCurrentUserName(), new Date(), "NotRequiredFormBean.notRequiredFormAction", notRequiredReason);
        pm.insertStatusChangeEvent(sce);
        
        // refresh event cache on controller
        controller.getEvents();
        
        logger.audit("In NotRequiredFormBean.notRequiredFormAction, after action, event status is " + controller.getSelectedEvent().getStatus());
        logger.audit("In NotRequiredFormBean.notRequiredFormAction, after action, form status is " + controller.getSelectedForm().getStatus());
        
        return controller.viewEventAction(event.getId());
    }

    public void setController(Controller c) {
        this.controller = c;
    }
    
    public Controller getController() {
        return controller;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
