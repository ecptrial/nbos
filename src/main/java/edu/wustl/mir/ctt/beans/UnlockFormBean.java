package edu.wustl.mir.ctt.beans;

import edu.wustl.mir.ctt.AEController;
import edu.wustl.mir.ctt.Controller;
import edu.wustl.mir.ctt.form.AdverseEventWorksheetSAEForm;
import edu.wustl.mir.ctt.form.BasicForm;
import edu.wustl.mir.ctt.model.ECPEventTypes;
import edu.wustl.mir.ctt.model.ECPEvents;
import edu.wustl.mir.ctt.model.ECPFormTypes;
import edu.wustl.mir.ctt.model.Event;
import edu.wustl.mir.ctt.model.EventStatus;
import edu.wustl.mir.ctt.model.FormStatus;
import edu.wustl.mir.ctt.model.StatusChangeEvent;
import edu.wustl.mir.ctt.notification.NotificationException;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.PersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import edu.wustl.mir.ctt.log.AuditLogger;

/**
 *
 * @author drm
 */
@ManagedBean
@RequestScoped
public class UnlockFormBean implements Serializable {
    private AuditLogger logger = AuditLogger.create(UnlockFormBean.class);
    private Event event;
    private String unlockReason;
    private Date dateOfOccurance;
    private String title;
    private BasicForm form;
    
    @ManagedProperty(value="#{controller}")
    private Controller controller;
    
    public UnlockFormBean() {
    }
    
    @PostConstruct
    public void init() {
        event = controller.getSelectedEvent();
        form = controller.getSelectedForm();
    }
    
    public String getUnlockReason() { return unlockReason; }
    public void setUnlockReason(String u) { unlockReason = u; }
    public BasicForm getForm() { return form; }
    public void setForm(BasicForm f) { form = f; }
    
    public String unlockFormAction() throws PersistenceException, NotificationException {
        logger.audit("In UnlockFormBean.unlockFormAction, before unlock, event status is " + controller.getSelectedEvent().getStatus());
        logger.audit("In UnlockFormBean.unlockFormAction, before unlock, form status is " + controller.getSelectedForm().getStatus());

        // do unlock
        PersistenceManager pm = ServiceRegistry.getPersistenceManager();
        
        pm.changeEventStatus(event, EventStatus.SUBMITTED);
        pm.changeFormStatus(form, FormStatus.SUBMITTED);
        
        StatusChangeEvent sce = new StatusChangeEvent(form.getId(), form.getStatus(), FormStatus.SUBMITTED, controller.getCurrentUserName(), new Date(), "UnlockFormBean.unlockFormAction", unlockReason);
        pm.insertStatusChangeEvent(sce);
        
        // refresh event cache on controller
        controller.getEvents();
        
        logger.audit("In UnlockFormBean.unlockFormAction, after unlock, event status is " + controller.getSelectedEvent().getStatus());
        logger.audit("In UnlockFormBean.unlockFormAction, after unlock, form status is " + controller.getSelectedForm().getStatus());
        
        return controller.viewEventAction(event.getId());
    }

    public void setController(Controller c) {
        this.controller = c;
    }
    
    public Controller getController() {
        return controller;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
